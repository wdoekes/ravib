-- MariaDB dump 10.19  Distrib 10.6.7-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ravib_tool
-- ------------------------------------------------------
-- Server version	10.6.7-MariaDB-2ubuntu1.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actors`
--

DROP TABLE IF EXISTS `actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `chance` tinyint(3) unsigned NOT NULL,
  `knowledge` tinyint(3) unsigned NOT NULL,
  `resources` tinyint(3) unsigned NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`organisation_id`),
  CONSTRAINT `actors_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `advisors`
--

DROP TABLE IF EXISTS `advisors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advisors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `private_key` text NOT NULL,
  `public_key` text NOT NULL,
  `crypto_key` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `organisation_id` (`organisation_id`),
  KEY `advisors_ibfk_2` (`user_id`),
  CONSTRAINT `advisors_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `advisors_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bia`
--

DROP TABLE IF EXISTS `bia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `item` text NOT NULL,
  `description` text NOT NULL,
  `impact` text NOT NULL,
  `availability` tinyint(3) unsigned NOT NULL,
  `integrity` tinyint(3) unsigned NOT NULL,
  `confidentiality` tinyint(3) unsigned NOT NULL,
  `owner` tinyint(3) unsigned NOT NULL,
  `personal_data` tinyint(1) NOT NULL,
  `location` enum('intern','extern','saas') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`organisation_id`),
  CONSTRAINT `bia_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `key` varchar(100) NOT NULL,
  `value` mediumtext NOT NULL,
  `timeout` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_progress`
--

DROP TABLE IF EXISTS `case_progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_progress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `executor_id` int(10) unsigned DEFAULT NULL,
  `reviewer_id` int(10) unsigned DEFAULT NULL,
  `measure_id` int(10) unsigned DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `info` text NOT NULL,
  `done` tinyint(1) NOT NULL,
  `hours_planned` smallint(5) unsigned NOT NULL,
  `hours_invested` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  KEY `progress_people_id` (`executor_id`),
  KEY `iso_measure_id` (`measure_id`),
  KEY `reviewer_id` (`reviewer_id`),
  CONSTRAINT `case_progress_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `case_progress_ibfk_2` FOREIGN KEY (`executor_id`) REFERENCES `users` (`id`),
  CONSTRAINT `case_progress_ibfk_3` FOREIGN KEY (`reviewer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `case_progress_ibfk_4` FOREIGN KEY (`measure_id`) REFERENCES `measures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_scenarios`
--

DROP TABLE IF EXISTS `case_scenarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_scenarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `title` text NOT NULL,
  `scenario` text NOT NULL,
  `consequences` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  CONSTRAINT `case_scenarios_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_scope`
--

DROP TABLE IF EXISTS `case_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_scope` (
  `case_id` int(10) unsigned NOT NULL,
  `bia_id` int(10) unsigned NOT NULL,
  KEY `case_id` (`case_id`),
  KEY `bia_id` (`bia_id`),
  CONSTRAINT `case_scope_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `case_scope_ibfk_2` FOREIGN KEY (`bia_id`) REFERENCES `bia` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_threat_bia`
--

DROP TABLE IF EXISTS `case_threat_bia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_threat_bia` (
  `case_threat_id` int(10) unsigned NOT NULL,
  `bia_id` int(10) unsigned NOT NULL,
  KEY `case_threat_id` (`case_threat_id`),
  KEY `case_bia_id` (`bia_id`),
  CONSTRAINT `case_threat_bia_ibfk_1` FOREIGN KEY (`case_threat_id`) REFERENCES `case_threats` (`id`),
  CONSTRAINT `case_threat_bia_ibfk_2` FOREIGN KEY (`bia_id`) REFERENCES `bia` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_threat_measure`
--

DROP TABLE IF EXISTS `case_threat_measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_threat_measure` (
  `case_threat_id` int(10) unsigned NOT NULL,
  `measure_id` int(10) unsigned NOT NULL,
  KEY `case_threat_id` (`case_threat_id`),
  KEY `measure_id` (`measure_id`),
  CONSTRAINT `case_threat_measure_ibfk_1` FOREIGN KEY (`case_threat_id`) REFERENCES `case_threats` (`id`),
  CONSTRAINT `case_threat_measure_ibfk_2` FOREIGN KEY (`measure_id`) REFERENCES `measures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_threats`
--

DROP TABLE IF EXISTS `case_threats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_threats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `threat` text NOT NULL,
  `actor_id` int(10) unsigned DEFAULT NULL,
  `chance` tinyint(4) NOT NULL,
  `impact` tinyint(4) NOT NULL,
  `handle` tinyint(4) NOT NULL,
  `action` text NOT NULL,
  `current` text NOT NULL,
  `argumentation` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  KEY `actor_id` (`actor_id`),
  CONSTRAINT `case_threats_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `case_threats_ibfk_2` FOREIGN KEY (`actor_id`) REFERENCES `actors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `standard_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `organisation` text NOT NULL,
  `date` date NOT NULL,
  `scope` text NOT NULL,
  `impact` text NOT NULL,
  `interests` text NOT NULL,
  `logo` varchar(250) DEFAULT NULL,
  `archived` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`organisation_id`),
  KEY `iso_standard_id` (`standard_id`),
  CONSTRAINT `cases_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `cases_ibfk_2` FOREIGN KEY (`standard_id`) REFERENCES `standards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `en` text NOT NULL,
  `nl` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page` (`page`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_page_views`
--

DROP TABLE IF EXISTS `log_page_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_page_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` tinytext NOT NULL,
  `date` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_referers`
--

DROP TABLE IF EXISTS `log_referers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_referers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostname` tinytext NOT NULL,
  `url` text NOT NULL,
  `date` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_visits`
--

DROP TABLE IF EXISTS `log_visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_visits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  `error` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `measure_categories`
--

DROP TABLE IF EXISTS `measure_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measure_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `standard_id` int(10) unsigned NOT NULL,
  `number` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_standard` (`standard_id`),
  CONSTRAINT `measure_categories_ibfk_1` FOREIGN KEY (`standard_id`) REFERENCES `standards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure_categories`
--
-- ORDER BY:  `id`

LOCK TABLES `measure_categories` WRITE;
/*!40000 ALTER TABLE `measure_categories` DISABLE KEYS */;
INSERT INTO `measure_categories` VALUES (1,1,5,'Informatiebeveiligingsbeleid'),(2,1,6,'Organiseren van informatiebeveiliging'),(3,1,7,'Veilig personeel'),(4,1,8,'Beheer van bedrijfsmiddelen'),(5,1,9,'Toegangsbeveiliging'),(6,1,10,'Cryptografie'),(7,1,11,'Fysieke beveiliging en beveiliging van de omgeving'),(8,1,12,'Beveiliging bedrijfsvoering'),(9,1,13,'Communicatiebeveiliging'),(10,1,14,'Acquisitie, ontwikkeling en onderhoud van informatiesystemen'),(11,1,15,'Leveranciersrelaties'),(12,1,16,'Beheer van informatiebeveiligingsincidenten'),(13,1,17,'Informatiebeveiligingsaspecten van bedrijfscontiniuïteitsbeheer'),(14,1,18,'Naleving'),(15,2,5,'Organisatorische beheersmaatregelen'),(16,2,6,'Mensgerichte beheersmaatregelen'),(17,2,7,'Fysieke beheersmaatregelen'),(18,2,8,'Technologische beheersmaatregelen');
/*!40000 ALTER TABLE `measure_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measures`
--

DROP TABLE IF EXISTS `measures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `standard_id` int(10) unsigned NOT NULL,
  `number` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `reduce` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_standard` (`standard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measures`
--
-- ORDER BY:  `id`

LOCK TABLES `measures` WRITE;
/*!40000 ALTER TABLE `measures` DISABLE KEYS */;
INSERT INTO `measures` VALUES (1,1,'5.1.1','Beleidsregels voor informatiebeveiliging',2),(2,1,'5.1.2','Beoordeling van het informatiebeveiligingsbeleid',2),(3,1,'6.1.1','Rollen en verantwoordelijkheden bij informatiebeveiliging',2),(4,1,'6.1.2','Scheiding van taken',0),(5,1,'6.1.3','Contact met overheidsinstanties',2),(6,1,'6.1.4','Contact met speciale belangengroepen',2),(7,1,'6.1.5','Informatiebeveiliging in projectbeheer',2),(8,1,'6.2.1','Beleid voor mobiele apparatuur',2),(9,1,'6.2.2','Telewerken',2),(10,1,'7.1.1','Screening',0),(11,1,'7.1.2','Arbeidsvoorwaarden',2),(12,1,'7.2.1','Directieverantwoordelijkheden',0),(13,1,'7.2.2','Bewustzijn, opleiding en training ten aanzien van informatiebeveiliging',2),(14,1,'7.2.3','Disciplinaire procedure',0),(15,1,'7.3.1','Beëindiging of wijziging van verantwoordelijkheden van het dienstverband',0),(16,1,'8.1.1','Inventariseren van bedrijfsmiddelen',2),(17,1,'8.1.2','Eigendom van bedrijfsmiddelen',2),(18,1,'8.1.3','Aanvaardbaar gebruik van bedrijfsmiddelen',0),(19,1,'8.1.4','Teruggeven van bedrijfsmiddelen',0),(20,1,'8.2.1','Classificatie van informatie',0),(21,1,'8.2.2','Informatie labelen',0),(22,1,'8.2.3','Behandelen van bedrijfsmiddelen',0),(23,1,'8.3.1','Beheer van verwijderbare media',0),(24,1,'8.3.2','Verwijderen van media',0),(25,1,'8.3.3','Media fysiek overdragen',2),(26,1,'9.1.1','Beleid voor toegangsbeveiliging',2),(27,1,'9.1.2','Toegang tot netwerken en netwerkdiensten',2),(28,1,'9.2.1','Registratie en afmelden van gebruikers',0),(29,1,'9.2.2','Gebruikers toegang verlenen',0),(30,1,'9.2.3','Beheren van speciale toegangsrechten',0),(31,1,'9.2.4','Beheer van geheime authenticatie-informatie van gebruikers',0),(32,1,'9.2.5','Beoordeling van toegangsrechten van gebruikers',0),(33,1,'9.2.6','Toegangsrechten intrekken of aanpassen',0),(34,1,'9.3.1','Geheime authenticatie-informatie gebruiken',0),(35,1,'9.4.1','Beperking toegang tot informatie',2),(36,1,'9.4.2','Beveiligde inlogprocedures',2),(37,1,'9.4.3','Systeem voor wachtwoordbeheer',0),(38,1,'9.4.4','Speciale systeemhulpmiddelen gebruiken',2),(39,1,'9.4.5','Toegangsbeveiliging op programmabroncode',2),(40,1,'10.1.1','Beleid inzake het gebruik van cryptografische beheersmaatregelen',2),(41,1,'10.1.2','Sleutelbeheer',2),(42,1,'11.1.1','Fysieke beveiligingszone',2),(43,1,'11.1.2','Fysieke toegangsbeveiliging',2),(44,1,'11.1.3','Kantoren, ruimten en faciliteiten beveiligen',2),(45,1,'11.1.4','Beschermen tegen bedreigingen van buitenaf',1),(46,1,'11.1.5','Werken in beveiligde gebieden',0),(47,1,'11.1.6','Laad- en loslocatie',0),(48,1,'11.2.1','Plaatsing en bescherming van apparatuur',0),(49,1,'11.2.2','Nutsvoorzieningen',0),(50,1,'11.2.3','Beveiliging van bekabeling',0),(51,1,'11.2.4','Onderhoud van apparatuur',0),(52,1,'11.2.5','Verwijdering van bedrijfsmiddelen',0),(53,1,'11.2.6','Beveiliging van apparatuur en bedrijfsmiddelen buiten het terrein',2),(54,1,'11.2.7','Veilig verwijderen of hergebruiken van apparatuur',0),(55,1,'11.2.8','Onbeheerde gebruikersapparatuur',0),(56,1,'11.2.9','\'Clear desk\'- en \'clear screen\'-beleid',0),(57,1,'12.1.1','Gedocumenteerde bedieningsprocedures',2),(58,1,'12.1.2','Wijzigingsbeheer',0),(59,1,'12.1.3','Capaciteitsbeheer',0),(60,1,'12.1.4','Scheiding van ontwikkel-, test- en productieomgevingen',0),(61,1,'12.2.1','Beheersmaatregelen tegen malware',0),(62,1,'12.3.1','Back-up van informatie',1),(63,1,'12.4.1','Gebeurtenissen registreren',2),(64,1,'12.4.2','Beschermen van informatie in logbestanden',2),(65,1,'12.4.3','Logbestanden van beheerders en operators',0),(66,1,'12.4.4','Kloksynchronisatie',0),(67,1,'12.5.1','Software installeren op operationele systemen',2),(68,1,'12.6.1','Beheer van technische kwetsbaarheden',2),(69,1,'12.6.2','Beperkingen voor het installeren van software',2),(70,1,'12.7.1','Beheersmaatregelen betreffende audits van informatiesystemen',2),(71,1,'13.1.1','Beheersmaatregelen voor netwerken',2),(72,1,'13.1.2','Beveiliging van netwerkdiensten',2),(73,1,'13.1.3','Scheiding in netwerken',0),(74,1,'13.2.1','Beleid en procedures voor informatietransport',0),(75,1,'13.2.2','Overeenkomsten over informatietransport',0),(76,1,'13.2.3','Elektronische berichten',0),(77,1,'13.2.4','Vertrouwelijkheids- of geheimhoudingsovereenkomst',2),(78,1,'14.1.1','Analyse en specificatie van informatiebeveiligingseisen',2),(79,1,'14.1.2','Toepassingen op openbare netwerken beveiligen',2),(80,1,'14.1.3','Transacties van toepassingen beschermen',2),(81,1,'14.2.1','Beleid voor beveiligd ontwikkelen',2),(82,1,'14.2.2','Procedures voor wijzigingsbeheer met betrekking tot systemen',2),(83,1,'14.2.3','Technische beoordeling van toepassingen na wijzigingen besturingsplatform',0),(84,1,'14.2.4','Beperkingen op wijzigingen aan softwarepakketten',0),(85,1,'14.2.5','Principes voor engineering van beveiligde systemen',2),(86,1,'14.2.6','Beveiligde ontwikkelomgeving',0),(87,1,'14.2.7','Uitbestede softwareontwikkeling',2),(88,1,'14.2.8','Testen van systeembeveiliging',2),(89,1,'14.2.9','Systeemacceptatietests',2),(90,1,'14.3.1','Bescherming van testgegevens',0),(91,1,'15.1.1','Informatiebeveiligingsbeleid voor leveranciersrelaties',2),(92,1,'15.1.2','Opnemen van beveiligingsaspecten in leveranciersovereenkomsten',2),(93,1,'15.1.3','Toeleveringsketen van informatie- en communicatietechnologie',2),(94,1,'15.2.1','Monitoring en beoordeling van dienstverlening van leveranciers',2),(95,1,'15.2.2','Beheer van veranderingen in dienstverlening van leveranciers',2),(96,1,'16.1.1','Verantwoordelijkheden en procedures',2),(97,1,'16.1.2','Rapportage van informatiebeveiligingsgebeurtenissen',2),(98,1,'16.1.3','Rapportage van zwakke plekken in de informatiebeveiliging',0),(99,1,'16.1.4','Beoordeling van en besluitvorming over informatiebeveiligingsgebeurtenissen',1),(100,1,'16.1.5','Respons op informatiebeveiligingsincidenten',1),(101,1,'16.1.6','Lering uit informatiebeveiligingsincidenten',2),(102,1,'16.1.7','Verzamelen van bewijsmateriaal',1),(103,1,'17.1.1','Informatiebeveiligingscontinuïteit plannen',1),(104,1,'17.1.2','Informatiebeveiligingscontinuïteit implementeren',1),(105,1,'17.1.3','Informatiebeveiligingscontinuïteit verifiëren, beoordelen en evalueren',1),(106,1,'17.2.1','Beschikbaarheid van informatieverwerkende faciliteiten',1),(107,1,'18.1.1','Vaststellen van toepasselijke wetgeving en contractuele eisen',2),(108,1,'18.1.2','Intellectuele-eigendomsrechten',2),(109,1,'18.1.3','Beschermen van registraties',2),(110,1,'18.1.4','Privacy en bescherming van persoonsgegevens',2),(111,1,'18.1.5','Voorschriften voor het gebruik van cryptografische beheersmaatregelen',2),(112,1,'18.2.1','Onafhankelijke beoordeling van informatiebeveiliging',2),(113,1,'18.2.2','Naleving van beveiligingsbeleid en -normen',2),(114,1,'18.2.3','Beoordeling van technische naleving',2),(115,2,'5.1','Beleidsregels voor informatiebeveiliging',2),(116,2,'5.2','Rollen en verantwoordelijkheden bij informatiebeveiliging',2),(117,2,'5.3','Functiescheiding',0),(118,2,'5.4','Managementverantwoordelijkheden',0),(119,2,'5.5','Contact met overheidsinstanties',2),(120,2,'5.6','Contact met speciale belangengroepen',2),(121,2,'5.7','Informatie over informatiebeveiligingsdreigingen',2),(122,2,'5.8','Informatiebeveiliging in projectmanagement',2),(123,2,'5.9','Inventarisatie van informatie en andere gerelateerde bedrijfsmiddelen',2),(124,2,'5.10','Aanvaard gebruik van informatie en andere gerelateerde bedrijfsmiddelen',0),(125,2,'5.11','Retourneren van bedrijfsmiddelen',0),(126,2,'5.12','Classificeren van informatie',0),(127,2,'5.13','Labelen van informatie',0),(128,2,'5.14','Overdragen van informatie',0),(129,2,'5.15','Toegangsbeveiliging',2),(130,2,'5.16','Identiteitsbeheer',0),(131,2,'5.17','Beheren van authenticatie informatie',0),(132,2,'5.18','Toegangsrechten',0),(133,2,'5.19','Informatiebeveiliging in leveranciersrelaties',2),(134,2,'5.20','Adresseren van informatiebeveiliging in leveranciersovereenkomsten',2),(135,2,'5.21','Beheren van informatiebeveiliging in de ICT-keten',2),(136,2,'5.22','Monitoren, beoordelen en het beheren van wijzigingen op van leveranciersdiensten',2),(137,2,'5.23','Informatiebeveiliging voor het gebruik van clouddiensten',0),(138,2,'5.24','Plannen en voorbereiden van het beheer van informatiebeveiligingsincidenten',2),(139,2,'5.25','Beoordelen van en besluiten over informatiebeveiligingsgebeurtenissen',1),(140,2,'5.26','Reageren op informatiebeveiligingsincidenten',1),(141,2,'5.27','Leren van informatiebeveiligingsincidenten',2),(142,2,'5.28','Verzamelen van bewijsmateriaal',1),(143,2,'5.29','Informatiebeveiliging tijdens een verstoring',1),(144,2,'5.30','ICT-gereedheid voor bedrijfscontinuïteit',0),(145,2,'5.31','Wettelijke, statutaire, regelgevende en contractuele eisen',2),(146,2,'5.32','Intellectuele eigendomsrechten',2),(147,2,'5.33','Beschermen van registraties',2),(148,2,'5.34','Privacy en bescherming van persoonsgegevens',2),(149,2,'5.35','Onafhankelijke beoordeling van informatiebeveiliging',2),(150,2,'5.36','Naleving van beleid, regels en normen voor informatiebeveiliging',2),(151,2,'5.37','Gedocumenteerde bedieningsprocedures',2),(152,2,'6.1','Screening',0),(153,2,'6.2','Arbeidsovereenkomst',2),(154,2,'6.3','Bewustwording, opleiding en training op informatiebeveiliging',2),(155,2,'6.4','Disciplinaire procedure',0),(156,2,'6.5','Verantwoordelijkheden na beëindiging of wijziging van het dienstverband',0),(157,2,'6.6','Vertrouwelijkheids- of geheimhoudingsovereenkomsten',2),(158,2,'6.7','Werken op afstand',2),(159,2,'6.8','Melden van informatiebeveiligingsgebeurtenissen',2),(160,2,'7.1','Fysieke beveiligingszones',2),(161,2,'7.2','Fysieke toegangsbeveiliging',2),(162,2,'7.3','Beveiligen van kantoren, ruimten en faciliteiten',2),(163,2,'7.4','Monitoren van de fysieke beveiliging',0),(164,2,'7.5','Beschermen tegen fysieke en omgevings dreigingen',1),(165,2,'7.6','Werken in beveiligde gebieden',0),(166,2,'7.7','\'Clear desk\' en \'clear screen\'',0),(167,2,'7.8','Plaatsen en beschermen van apparatuur',0),(168,2,'7.9','Beveiligen van bedrijfsmiddelen buiten het terrein',2),(169,2,'7.10','Opslagmedia',2),(170,2,'7.11','Nutsvoorzieningen',0),(171,2,'7.12','Beveiligen van bekabeling',0),(172,2,'7.13','Onderhoud van apparatuur',0),(173,2,'7.14','Veilig verwijderen of hergebruiken van apparatuur',0),(174,2,'8.1','Gebruikersapparatuur',2),(175,2,'8.2','Speciale toegangsrechten',0),(176,2,'8.3','Beperking toegang tot informatie',2),(177,2,'8.4','Toegangsbeveiliging op broncode',2),(178,2,'8.5','Beveiligde authenticatie',2),(179,2,'8.6','Capaciteitsbeheer',0),(180,2,'8.7','Bescherming tegen malware',0),(181,2,'8.8','Beheer van technische kwetsbaarheden',2),(182,2,'8.9','Configuratiebeheer',0),(183,2,'8.10','Wissen van informatie',0),(184,2,'8.11','Maskeren van gegevens',0),(185,2,'8.12','Voorkomen van gegevenslekken',0),(186,2,'8.13','Back-up van informatie',1),(187,2,'8.14','Redundantie van informatieverwerkende faciliteiten',1),(188,2,'8.15','Gebeurtenisregistratie',2),(189,2,'8.16','Monitoren van activiteiten',0),(190,2,'8.17','Kloksynchronisatie',0),(191,2,'8.18','Gebruik van speciale systeemhulpmiddelen',2),(192,2,'8.19','Installeren van software op operationele systemen',2),(193,2,'8.20','Beveiliging netwerkcomponenten',2),(194,2,'8.21','Beveiliging van netwerkdiensten',2),(195,2,'8.22','Netwerksegmentatie',0),(196,2,'8.23','Toepassen van webfilters',0),(197,2,'8.24','Gebruik van cryptografie',2),(198,2,'8.25','Beveiligen tijdens de ontwikkelcyclus',0),(199,2,'8.26','Toepassingsbeveiligingseisen',2),(200,2,'8.27','Principes voor de engineering van beveiligde systemen en systeemarchitecturen',2),(201,2,'8.28','Veilige software ontwikkelen',2),(202,2,'8.29','Testen van de beveiliging tijdens ontwikkeling en acceptatie',2),(203,2,'8.30','Uitbestede systeemontwikkeling',0),(204,2,'8.31','Scheiding van ontwikkel-, test- en productieomgevingen',0),(205,2,'8.32','Wijzigingsbeheer',2),(206,2,'8.33','Testgegevens',0),(207,2,'8.34','Bescherming van informatiesystemen tijdens audit',2);
/*!40000 ALTER TABLE `measures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `text` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--
-- ORDER BY:  `id`

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,NULL,'Dashboard','/dashboard'),(2,NULL,'Documentatie','/documentatie'),(3,NULL,'Risicomatrix','/risicomatrix'),(4,NULL,'Koppelingen','/koppelingen'),(5,NULL,'Uitloggen','/uitloggen');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mitigation`
--

DROP TABLE IF EXISTS `mitigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mitigation` (
  `measure_id` int(10) unsigned NOT NULL,
  `threat_id` int(10) unsigned NOT NULL,
  KEY `iso_measure_id` (`measure_id`),
  KEY `threat_id` (`threat_id`),
  CONSTRAINT `mitigation_ibfk_1` FOREIGN KEY (`measure_id`) REFERENCES `measures` (`id`),
  CONSTRAINT `mitigation_ibfk_2` FOREIGN KEY (`threat_id`) REFERENCES `threats` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mitigation`
--

LOCK TABLES `mitigation` WRITE;
/*!40000 ALTER TABLE `mitigation` DISABLE KEYS */;
INSERT INTO `mitigation` VALUES (3,3),(10,3),(11,3),(13,3),(14,3),(7,59),(57,59),(58,59),(59,59),(82,59),(83,59),(84,59),(85,59),(91,59),(93,59),(94,59),(95,59),(13,11),(26,11),(27,11),(31,11),(34,11),(61,11),(62,11),(68,11),(69,11),(73,11),(93,11),(114,11),(39,57),(60,57),(81,57),(86,57),(87,57),(88,57),(89,57),(90,57),(59,12),(89,12),(106,12),(8,38),(9,38),(61,38),(71,38),(72,38),(73,38),(91,38),(48,41),(51,41),(62,41),(93,41),(106,41),(57,40),(58,40),(70,40),(78,40),(82,40),(83,40),(84,40),(93,40),(60,39),(67,39),(78,39),(82,39),(83,39),(84,39),(85,39),(88,39),(89,39),(93,39),(7,56),(17,56),(58,56),(67,56),(68,56),(78,56),(82,56),(84,56),(87,56),(95,56),(7,42),(13,42),(30,42),(62,42),(11,16),(13,16),(14,16),(18,16),(63,16),(64,16),(67,16),(69,16),(113,16),(8,17),(10,17),(16,17),(19,17),(43,17),(44,17),(45,17),(47,17),(48,17),(52,17),(53,17),(62,17),(11,18),(14,18),(10,19),(44,19),(77,19),(91,19),(8,37),(23,37),(25,37),(40,37),(53,37),(77,37),(10,20),(13,20),(14,20),(28,20),(30,20),(31,20),(37,20),(55,20),(56,20),(64,20),(80,20),(10,22),(14,22),(15,22),(18,22),(28,22),(30,22),(65,22),(66,22),(77,22),(91,22),(3,2),(5,2),(13,2),(16,2),(18,2),(20,2),(112,2),(113,2),(13,23),(31,23),(34,23),(36,23),(37,23),(13,24),(53,24),(55,24),(56,24),(72,24),(17,25),(18,25),(20,25),(21,25),(22,25),(26,25),(27,25),(79,25),(90,25),(17,26),(23,26),(24,26),(25,26),(52,26),(54,26),(6,28),(61,28),(64,28),(68,28),(73,28),(78,28),(82,28),(83,28),(84,28),(85,28),(88,28),(89,28),(93,28),(94,28),(114,28),(6,29),(36,29),(50,29),(68,29),(71,29),(73,29),(88,29),(89,29),(93,29),(94,29),(7,58),(70,58),(91,58),(93,58),(94,58),(95,58),(9,30),(52,30),(53,30),(42,31),(43,31),(47,31),(48,31),(50,31),(61,31),(69,31),(14,32),(20,32),(21,32),(23,32),(25,32),(40,32),(77,32),(79,32),(80,32),(93,32),(25,33),(40,33),(77,33),(79,33),(23,35),(62,35),(69,35),(40,36),(79,36),(80,36),(91,36),(40,27),(41,27),(111,27),(93,7),(94,7),(95,7),(107,7),(8,8),(9,8),(13,8),(107,8),(111,8),(40,9),(41,9),(107,9),(111,9),(61,13),(63,13),(96,13),(97,13),(98,13),(99,13),(100,13),(16,14),(63,14),(64,14),(65,14),(66,14),(99,14),(100,14),(101,14),(102,14),(103,14),(104,14),(42,44),(43,44),(44,44),(45,44),(46,44),(47,44),(48,44),(56,44),(42,45),(43,45),(44,45),(45,45),(48,45),(49,45),(56,45),(62,45),(103,45),(104,45),(105,45),(106,45),(42,46),(45,46),(48,46),(49,46),(56,46),(62,46),(103,46),(104,46),(105,46),(106,46),(103,47),(104,47),(105,47),(42,48),(45,48),(48,48),(56,48),(62,48),(103,48),(104,48),(105,48),(106,48),(49,49),(50,49),(106,49),(45,50),(48,50),(49,50),(50,50),(7,52),(57,52),(78,52),(95,52),(106,52),(7,43),(57,43),(78,43),(87,43),(95,43),(57,53),(95,53),(2,1),(17,1),(103,1),(112,1),(113,1),(1,1),(14,15),(96,15),(97,15),(101,15),(113,15),(15,21),(17,21),(19,21),(26,21),(27,21),(28,21),(29,21),(30,21),(31,21),(32,21),(33,21),(35,21),(38,21),(39,21),(60,21),(115,1),(116,2),(116,3),(119,2),(120,28),(120,29),(124,2),(124,16),(124,22),(124,25),(127,25),(127,32),(126,2),(126,25),(126,32),(129,11),(129,21),(129,25),(125,17),(125,21),(152,3),(152,17),(152,19),(152,20),(152,22),(155,3),(155,16),(155,18),(155,20),(155,22),(155,32),(155,15),(156,22),(156,21),(158,38),(158,30),(158,8),(153,3),(153,16),(153,18),(154,2),(154,3),(154,11),(154,42),(154,16),(154,20),(154,23),(154,24),(154,8),(109,25),(109,35),(147,25),(147,35),(149,1),(149,2),(151,59),(151,40),(151,52),(151,43),(151,53),(166,20),(166,24),(166,44),(166,45),(166,48),(166,46),(168,17),(168,37),(168,24),(168,30),(173,26),(172,41),(171,29),(171,31),(171,49),(171,50),(165,44),(160,31),(160,44),(160,45),(160,48),(160,46),(161,17),(161,31),(161,44),(161,45),(162,17),(162,19),(162,44),(162,45),(164,17),(164,44),(164,45),(164,48),(164,46),(164,50),(170,45),(170,46),(170,49),(170,50),(167,41),(167,17),(167,31),(167,44),(167,45),(167,48),(167,46),(167,50),(139,13),(139,14),(122,59),(122,40),(122,39),(122,56),(122,42),(122,28),(122,58),(122,52),(122,43),(123,1),(123,2),(123,56),(123,17),(123,21),(123,25),(123,26),(123,14),(130,20),(130,22),(130,21),(131,11),(131,20),(131,21),(131,23),(132,21),(133,59),(133,38),(133,19),(133,22),(133,58),(133,36),(92,59),(92,12),(92,38),(92,19),(92,22),(92,58),(134,59),(134,12),(134,38),(134,19),(134,22),(134,58),(135,59),(135,11),(135,41),(135,40),(135,39),(135,28),(135,29),(135,58),(135,32),(135,7),(136,59),(136,56),(136,28),(136,29),(136,58),(136,7),(136,52),(136,43),(136,53),(138,13),(138,15),(140,13),(140,14),(141,14),(141,15),(142,14),(143,1),(143,14),(143,44),(143,45),(143,48),(143,46),(143,47),(145,27),(145,7),(145,8),(145,9),(150,1),(150,2),(150,11),(150,16),(150,28),(150,15),(157,19),(157,37),(157,22),(157,32),(157,33),(159,13),(159,15),(169,37),(169,26),(169,32),(169,33),(169,35),(174,38),(174,17),(174,37),(174,20),(174,24),(174,8),(175,42),(175,20),(175,22),(175,21),(176,21),(177,57),(177,21),(178,23),(178,29),(179,59),(179,12),(180,11),(180,38),(180,28),(180,31),(180,13),(181,11),(181,56),(181,28),(181,29),(186,11),(186,41),(186,42),(186,17),(186,35),(186,45),(186,48),(186,46),(187,12),(187,41),(187,45),(187,48),(187,46),(187,49),(187,52),(188,16),(188,20),(188,22),(188,28),(188,13),(188,14),(190,22),(190,14),(191,21),(192,39),(192,56),(192,16),(193,38),(193,29),(194,38),(194,24),(195,11),(195,38),(195,28),(195,29),(197,37),(197,32),(197,33),(197,36),(197,27),(197,9),(198,57),(199,20),(199,25),(199,32),(199,33),(199,36),(202,57),(202,39),(202,28),(202,29),(203,57),(203,56),(203,43),(204,57),(204,39),(204,21),(205,59),(205,40),(205,39),(205,56),(205,28),(206,57),(206,25),(207,40),(207,58),(200,59),(200,39),(200,28),(117,22),(118,1),(118,2),(118,3),(118,18),(4,22),(12,1),(12,2),(12,3),(12,18),(128,20),(128,25),(128,32),(128,33),(128,36),(74,25),(74,32),(74,33),(74,36),(75,20),(75,32),(75,36),(76,20),(76,32),(76,33),(137,59),(137,12),(137,58),(137,30),(137,7),(137,52),(201,59),(201,39),(201,28),(201,43),(196,11),(196,42),(196,16),(196,28),(144,59),(144,45),(144,48),(144,46),(144,47),(144,49),(144,52),(144,43),(146,57),(146,16),(108,57),(108,16),(183,17),(183,37),(183,26),(182,40),(182,21),(185,42),(185,17),(185,37),(185,20),(185,22),(185,21),(185,23),(185,24),(185,25),(185,26),(185,28),(185,29),(185,31),(185,32),(185,33),(184,27),(189,22),(189,28),(189,29),(189,13),(189,14),(163,17),(163,19),(163,20),(163,44),(163,50),(121,11),(121,12),(121,28),(121,29);
/*!40000 ALTER TABLE `mitigation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisations`
--

DROP TABLE IF EXISTS `organisations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `persistent` tinyint(1) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisations`
--
-- ORDER BY:  `id`

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;
INSERT INTO `organisations` VALUES (1,'Mijn organisatie',1,'2022-01-01 00:00:00');
/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_access`
--

DROP TABLE IF EXISTS `page_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_access` (
  `page_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`page_id`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `page_access_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  CONSTRAINT `page_access_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `language` varchar(2) NOT NULL,
  `layout` varchar(100) DEFAULT NULL,
  `private` tinyint(1) NOT NULL,
  `style` text DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `back` tinyint(1) NOT NULL,
  `form` tinyint(1) NOT NULL,
  `form_submit` varchar(32) DEFAULT NULL,
  `form_email` varchar(100) DEFAULT NULL,
  `form_done` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--
-- ORDER BY:  `id`

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'/documentatie','nl',NULL,0,'ol {\r\n  padding-left:25px;\r\n}\r\n\r\nol ol {\r\n  list-style-type: lower-alpha;\r\n}','Documentatie','','','<h2>Het uitvoeren van een risicoanalyse met RAVIB</h2>\r\n<p>RAVIB is een tool die ondersteuning biedt bij een risicoanalyse voor informatiebeveiliging. Het doel is het selecteren van maatregelen uit de gekozen beveiligingsnorm op basis van dreigingen die middels een workshop in kaart zijn gebracht. Het gebruik van RAVIB gaat uit van het doorlopen van een aantal stappen, zoals hieronder aangegeven.</p>\r\n\r\n<ol>\r\n<li><a href=\"/documentatie/bia\">Het in kaart brengen van relevantie informatiesystemen binnen uw organisatie.</a></li>\r\n<li><a href=\"/documentatie/actoren\">Het in kaart brengen van de actoren die een inbreuk kunnen maken op uw informatiebeveiliging.</a></li>\r\n<li><a href=\"/documentatie/risicoanalyse\">Het uitvoeren van de daadwerkelijke risicoanalyse:</a>\r\n<ol>\r\n<li><a href=\"/documentatie/scope\">Het bepalen van de scope van de risicoanalyse.</a></li>\r\n<li><a href=\"/documentatie/belangen\">Het vastleggen van de te beschermen belangen.</a></li>\r\n<li><a href=\"/documentatie/dreigingen\">Het doornemen van de dreigingen.</a></li>\r\n<li><a href=\"/documentatie/maatregelen\">Het bepalen van de mitigerende maatregelen.</a></li>\r\n<li><a href=\"/documentatie/scenarios\">Het uitwerken van mogelijke scenario\'s.</a></li>\r\n<li><a href=\"/documentatie/rapportage\">Opstellen van de rapportage.</a></li>\r\n<li><a href=\"/documentatie/voortgang\">Het bewaken van de voortgang van de maatregelenimplementatie.</a></li>\r\n</ol></li>\r\n</ol>\r\n\r\n<h2>Overige functionaliteiten</h2>\r\n<p>RAVIB bevat nog enkele functionaliteiten die niet direct betrekking hebben op het uitvoeren van een risicoanalyse. Deze staan hieronder uitgelegd.</p>\r\n<ul>\r\n<li><a href=\"/documentatie/gebruikersbeheer\">Het beheren van accounts voor overige gebruikers.</a></li>\r\n<li><a href=\"/documentatie/databeheer\">Het beheren van uw risicoanalyse-informatie.</a></li>\r\n<li><a href=\"/documentatie/adviseur\">Werken met RAVIB als informatiebeveiligingsadviseur.</a></li>\r\n</ul>',1,0,0,NULL,NULL,NULL),(2,'/documentatie/bia','nl',NULL,0,NULL,'Documentatie - BIA','','','<p>Tijdens een business impact analyse (BIA) worden de relevante informatiesystemen van de organisatie in kaart gebracht en per systeem bepaald in welk opzicht en in welke mate deze belangrijk is voor de organisatie. Bespreek wat de impact is voor de organisatie in het geval van een probleem met de beschikbaarheid, integriteit en/of vertrouwelijkheid van het systeem en de daarin opgeslagen informatie.<p>\r\n\r\n<p>De systeemeigenaar is de aangewezen persoon om de classificatie van de in een systeem opgeslagen informatie te bepalen. Dit vereist dat voor ieder informatiesysteem een eigenaar is aangewezen.</p>\r\n\r\n<p>In de ideale situatie is de BIA buiten de risicoanalyse om gedaan en beschikt de organisatie reeds over een overzicht van alle informatiesystemen en op welke manier deze belangrijk zijn voor de organisatie. Deze stap is dan niet meer dan het overnemen van de benodigde informatie uit het centrale informatiesysteemoverzicht. De praktijk wijst helaas vaak anders uit.</p>\r\n\r\n<p>In deze stap hoef je je dus niet te beperken tot de informatiesystemen die binnen de scope van een eerste risicoanalyse vallen. Voer bij voorkeur alle informatiesystemen in die mogelijk binnen de scope van een toekomstige risicoanalyse vallen. Het kiezen van de scope van een risicoanalyse wordt namelijk in een andere stap gedaan.</p>',1,1,0,NULL,NULL,NULL),(3,'/documentatie/actoren','nl',NULL,0,NULL,'Documentatie - Actoren','','','<p>In deze stap worden de actoren in kaart gebracht die een bedreiging vormen voor de informatiebeveiliging. Hun acties kunnen onbedoeld leiden tot een incident door gebrek aan kundigheid of interesse op het gebied van informatiebeveiliging of omdat zij bewust en doelgericht een digitale aanval uitvoeren.</p>\r\n\r\n<p>Op basis van de opgegeven bereidheid, kennisniveau en de middelen, bepaald RAVIB het dreigingsniveau dat van een actor uitgaat. Indien voor een actor geen middelen wordt opgegeven, gaat RAVIB er vanuit dat de actor geen kwade bedoelingen heeft en dat een incident veroorzaakt door deze actor niet bedoeld was.</p>\r\n\r\n<p>Het overzicht van mogelijke actoren is een noodzakelijk hulpmiddel bij het inschatten van de kans van een dreiging tijdens de risicoanalyse. De daarvoor beschikbare handout, die via de scope-pagina gemaakt kan worden, bevat dit overzicht.</p>',1,1,0,NULL,NULL,NULL),(4,'/documentatie/scope','nl',NULL,0,NULL,'Documentatie - Scope','','','<p>De eerste stap die genomen moet worden voordat men kan beginnen aan een risicoanalyse is het bepalen van de scope. Zeker bij grote organisaties is het ondoenlijk om één risicoanalyse voor de gehele organisatie uit te voeren. Het dan verstandiger om meerdere risicoanalyses uit te voeren waarbij je je per risicoanalyse richt op een beperkt onderdeel van de organisatie.</p>\r\n\r\n<p>Indien dit de eerste keer is dat je een risicoanalyse uitvoert, richt je dan op de vitale processen van de organisatie. De vitale processen van een organisatie zijn de processen die ervoor zorgen dat de producten of diensten die een organisatie levert, ook daadwerkelijk geleverd kunnen worden. Neem de informatiesystemen van een vitaal proces als scope voor een risicoanalyse. Maak zelf een afweging of bepaalde vitale processen gezamenlijk in een risicoanalyse behandeld kunnen worden.</p>\r\n\r\n<p>Concreet betekent het bepalen van de scope, bepalen welke informatiesystemen je meeneemt in de risicoanalyse. De scope kan bestaan uit de informatiesystemen behorende bij bijvoorbeeld een proces, een afdeling, een project of een om andere reden bij elkaar horende verzameling van informatiesystemen. Wees voorzichtig met het te ruim kiezen van je scope. Het gevaar van een te ruime scope is dat je daardoor niet diep genoeg op belangrijke details ingaat en dus een te oppervlakkig beeld krijgt van de feitelijke risico’s.</p>',1,1,0,NULL,NULL,NULL),(5,'/documentatie/risicoanalyse','nl',NULL,0,NULL,'Documentatie - Risicoanalyse','','','<h2>De voorbereiding</h2>\r\n<p>Het is zeer belangrijk om te beseffen dat tijdens een risicoanalyse kennis over welke risico’s een organisatie loopt niet kan voortkomen uit een hulpmiddel of een methodiek, maar slechts uit de mensen die aanwezig zijn bij de risicoanalyse. Zij zijn namelijk degene die weten wat er speelt. Een risicoanalyse is niet meer dan een manier om deze kennis op een gestructureerde manier te verzamelen. De risicoanalyse valt of staat bij de selectie van de deelnemers. Ga dus opzoek naar mensen die goed zicht hebben op wat echt belangrijk is voor de organisatie, maar daarbij nog voldoende zicht hebben op wat er speelt op de werkvloer. Ga opzoek naar mensen die verantwoordelijk zijn voor de zaken die binnen de gekozen scope vallen, mensen die direct de nadelen ondervinden van problemen die zich binnen de gekozen scope voordoen. Realiseer je daarbij dat personen die goed zijn in het inschatten van de kans op een incident, niet per se de personen die ook de juiste impact ervan kunnen inschatten en andersom. Vaak zijn mensen uit de business beter in het inschatten van de impact en techneuten beter in het inschatten van de kans.</p>\r\n\r\n<p>Voorafgaand aan de risicoanalyse dient aan de waarden voor impact een bedrag gekoppeld te worden. Dit is in RAVIB niet vast ingevuld, omdat dit voor iedere organisatie anders is. Een schadepost van €10.000,- kan voor een kleine onderneming een groot bedrag zijn en voor een multinational een niet noemenswaardig bedrag. Deze waarden kunnen het beste bepaald worden met iemand met gedegen kennis van de financiële situatie van de organisatie. Hierbij is het belangrijk om te beseffen dat deze waarden niet bedoeld zijn om een schadebedrag aan de uiteindelijke risico\'s te koppelen, maar slechts om de impact van een risico goed te kunnen plaatsen ten opzichte van de impact van de andere risico\'s. De impact hoeft per risico dan ook niet met een berekening of harde cijfers aangetoond te worden. Een goed onderbouwd gevoel is voldoende. Een goed overwogen impact is ook belangrijk om een risicoanalyse op een later moment te kunnen herhalen en de resultaten te kunnen vergelijken met de eerder uitgevoerde analyse.</p>\r\n\r\n<h2>Een eerste bijeenkomst</h2>\r\n<p>Een goede risicoanalyse doe je niet in tien minuten. De daarvoor benodigde tijd ligt meer in de buurt van een halve dag. De daadwerkelijk benodigde tijd is uiteraard afhankelijk van de gekozen scope, het aantal deelnemers en hun ervaring met het uitvoeren van een risicoanalyse. Het is daarom belangrijk dat de deelnemers goed beseffen wat er van hen verwacht wordt. Spreek met hen het proces door en geef ze een beeld van het soort vragen dat hen te wachten staat. Voer alleen een risicoanalyse uit met mensen die bereid zijn deze hoeveelheid tijd en energie erin te steken, anders is het zonde van de moeite.</p>\r\n\r\n<p>Hoewel de scope van de risicoanalyse een proces of een afdeling kan zijn, voer je de risicoanalyse uit op de daarbij behorende informatie en informatiesystemen. Want hoewel een risico kan voortkomen uit, bijvoorbeeld, een verkeerd ingericht of ontbrekend proces, dient gekeken te worden naar de risico’s ten aanzien van informatie. We hebben het hier ten slotte over informatiebeveiliging en niet over procesbeveiliging.</p>',1,1,0,NULL,NULL,NULL),(6,'/documentatie/dreigingen','nl',NULL,0,NULL,'Documentatie - Dreigingsanalyse','','','<p>Tijdens de dreigingsanalyse worden de dreigingen benoemd die relevant zijn voor de organisatie. Daarbij wordt bepaald wat de kans is dat een dreiging leidt tot een incident en wat de impact is van zo\'n incident. Vervolgens dient bepaald te worden hoe omgegaan moet worden met het vastgestelde risico. RAVIB beschikt over een lijst van algemene dreigingen (templates). Het gebruik van deze lijst is uiteraard niet verplicht. De lijst is bedoeld als inspiratiebron. Indien een ISO 27001 certificering het doel is, is het doorlopen van deze gehele templatelijst het overwegen waard.</p>\r\n\r\n<p>Aan het eind van het opstellen van de scope kan een handout gemaakt worden. Deze uitleg gaat er vanuit dat de deelnemers beschikken over deze handout.</p>\r\n\r\n<p>Het eerste dat je bepaalt bij een dreiging is de mogelijke actor. Ga daarbij na welke actor baat heeft bij het verkrijgen van toegang tot informatie of het schaden van de organisatie. Niet iedere dreiging heeft een actor. Soms treed een incident op omdat je gewoon pech hebt. Hardware gaat bijvoorbeeld wel eens stuk. Zijn meerdere actoren mogelijk, ga dan uit van de meest bedreigende actor.</p>\r\n\r\n<p>Ga bij het bepalen van de kans uit van de dreiging die uitgaat van de gekozen actor en de (mogelijke) aanwezigheid van kwetsbaarheden in de betreffende systemen. De vertaling van de dreiging vanuit de actor naar de kans-factor kan bepaald worden zoals aangegeven op de handout. Dit is echter de kans die hoort bij het bruto risico. Waar we naar opzoek zijn is de kans waarbij rekening is gehouden met de reeds genomen maatregelen. Dit is het netto risico. Het doel is namelijk dat uiteindelijk voor iedere dreiging het restrisico geaccepteerd kan worden doordat afdoende maatregelen genomen zijn.</p>\r\n\r\n<p>Bij het verlagen van de kans als gevolg van genomen maatregelen, stelt RAVIB de volgende regel voor. Heb je voldoende maatregelen genomen, ga dan maximaal twee niveau\'s omlaag in de kans. Zijn er wel maatregelen genomen, maar is er ruimte voor verbetering, ga dan maximaal een niveau omlaag. De reden hiervoor is dat het niet realistisch is om te denken dat een geavanceerde actor die het op jou voorzien heeft, buiten de deur gehouden kan worden. Wil je het risico verder verlagen, dan moet je dus impactverlagende maatregelen nemen. Vaak blijkt dat als een actor eenmaal binnen is, deze vrij spel heeft. Te veel organisaties nemen te weinig impactverlagende maatregelen. Met deze regel probeert RAVIB daar wat aan te doen. Uiteraard staat het je vrij om van deze regel af te wijken.</p>\r\n\r\n<p>Is voor de dreiging geen actor nodig of is de actor geen kwaadwillende actor, bekijk dan hoe vaak een incident als gevolg van de dreiging zou kunnen optreden.</p>\r\n\r\n<p>De impact wordt bepaald door alle mogelijke gevolgen van een incident. Denk aan de mogelijke gevolgen voor de beschikbaarheid, integriteit en vertrouwelijkheid van de getroffen informatie, de imagoschade, financiële schade of bestuurlijke / politieke gevolgen.</p>\r\n\r\n<p>Neem ook dreigingen met een laag risico op in het overzicht, ook al accepteer je het risico. Deze zijn namelijk mogelijk bruikbaar bij het opstellen van een scenario in een volgende stap.</p>\r\n\r\n<p>Het is goed om je te realiseren dat een risico meestal geen stip op de risicomatrix is, maar een lijn. Een risico kan zich met een kleine impact manifesteren of met een grote impact. Risico\'s met een lage impact hebben doorgaans een grotere kans dan die met een hoge impact. Neem bijvoorbeeld het ontvangen van phishing mail. Een mailserver ontvangt dagelijks vele phishingmails, maar de meeste daarvan worden door filters tegengehouden. Soms komt er wat relatief onschadelijke phishingmail door, maar doorzien gebruikers het of is het achterliggende webformulier door de hostingpartij al verwijderd. Heel soms is het raak en vullen gebruikers daarwerkelijk vertrouwelijke gegevens in. Deze situaties zijn in een lijn op de risicomatrix te plotten. Het is mogelijk om met maatregelen de gehele lijn te verlagen, maar wellicht wil je je als eerst richten op het middenste deel van de lijn en daarna op het rechter deel van de lijn.</p>\r\n\r\n<p>Bij de aanpak betekent \'beheersen\' het verlagen van zowel de kans als de impact, \'ontwijken\' het verlagen van de kans, \'verweren\' het verlagen van de impact en \'accepteren\' het niet nemen van verdere acties om het risico te verlagen. Het kiezen van \'accepteren\' is alleen zinvol als ervoor gekozen wordt om de gehele lijst van dreigingentemplates door te werken.</p>\r\n\r\n<p>Per dreiging zijn drie invulvelden beschikbaar; \'Gewenste situatie / te nemen acties\', \'Huidige situatie / huidige maatregelen\' en \'Argumentatie voor gemaakte keuze\'. Deze velden kunnen gebruikt worden voor respectievelijk een nulmeting, het latere plan van aanpak en argumentatie over de gekozen kans, impact en aanpak. De argumentatie is belangrijke informatie bij een eventuele certificering. De inhoud van deze velden is daardoor belangrijker dan de kans, impact en aanpak velden. Deze laatste geven in feite niet meer dan een prioritering of urgentie aan.</p>',1,1,0,NULL,NULL,NULL),(7,'/documentatie/maatregelen','nl',NULL,0,NULL,'Documentatie - Maatregelen','','','<p>Nadat de dreigingen zijn geïdentificeerd, moet voor iedere dreiging passende maatregelen worden gekozen. De beschikbare maatregelen zijn afkomstig uit de voor deze casus gekozen standaard, bijvoorbeeld de NEN-ISO/IEC 27002. Het is raadzaam om voor deze stap de gekozen standaard erbij te houden. Het is verstandig om deze stap uit te voeren met of door iemand met voldoende kennis van de gekozen standaard.</p>\r\n\r\n<p>Een maatregel kan kans-verlagend, impact-verlagend of beide zijn. Afhankelijk van de gekozen aanpak voor een dreiging, zijn maatregelen daarmee in lijn of niet. Indien men bijvoorbeeld heeft gekozen voor een ontwijkende aanpak (het verlagen van de kans), dan zijn de impact verlagende maatregelen daarmee niet in lijn. Deze kunnen alsnog worden geselecteerd, maar de tekst van de maatregelen is doorgestreept om het niet-in-lijn-zijn van de maatregel duidelijk te maken.</p>\r\n\r\n<p>Bovenaan de maatregelenlijst staat de naam van de norm waar deze maatregelen uit afkomst zijn. Rechts daarvan staat een kruis-symbool. Indien men daarop klikt, dan verschijnt een pulldown met daarin de in RAVIB aanwezige dreigingen-templates. Het selecteren van een van deze dreigingen, zorgt ervoor dat de volgens RAVIB mitigerende maatregelen worden geaccentueerd. De selectie wordt dus niet aangepast.</p>\r\n\r\n<p>Een uitroepteken-icoontje geeft een conflict aan. Dit is als u maatregelen selecteert voor een dreiging die u geaccepteerd heeft of als u bij een dreiging die u niet geaccpeteerd heeft nog geen maatregelen heeft geselecteerd.</p>',1,1,0,NULL,NULL,NULL),(8,'/documentatie/rapportage','nl',NULL,0,NULL,'Documentatie - Rapportage','','','<p>Is de risicoanalyse afgerond, dan kan de rapportage worden opgemaakt. De rapportage bevat een overzicht van de uitgevoerde risicoanalyse en geeft geen oordeel over het resultaat ervan.</p>',1,1,0,NULL,NULL,NULL),(9,'/documentatie/voortgang','nl',NULL,0,NULL,'Documentatie - Voortgang','','','<p>De laatste en tevens optionele stap die RAVIB biedt, is het bewaken van de voortgang van de maatregel-implementatie. Per maatregelen kunnen taken worden uitgezet. De persoon aan wie een taak wordt toegewezen, hoeft uiteraard de taak niet per se zelf uit te voeren, maar slechts ervoor te zorgen dat het uitgevoerd wordt. Een tweede persoon kan worden aangewezen om een zoveel mogelijk onafhankelijke toetsing te doen op een correcte implementatie van de maatregel.</p>\r\n\r\n<p>Bij het vastleggen van een taak, kan gekozen worden om notificaties per e-mail te versturen. RAVIB stuurt een herinnering van de taak per e-mail bij het verstrijken van de deadline. De controleur van een maatregel kan via een link in de ontvangen mail een taak als zijnde \'gereed\' markeren.</p>',1,1,0,NULL,NULL,NULL),(10,'/documentatie/databeheer','nl',NULL,0,NULL,'Documentatie - Databeheer','','','<h2>Exporteren van uw data</h2>\r\n<p>De databeheer-sectie in RAVIB biedt u de mogelijkheid om uw risicoanalyses veilig te stellen middels een export. De informatie die in de export wordt meegenomen zijn de informatiesystemen uit de BIA, de actoren en de risicoanalyses. Wat niet wordt geëxporteerd zijn de gebruikers.</p>\r\n\r\n<p>De export is een GZip-bestand met daarin een kopie van de data in JSON. De data is digitaal ondertekend, waar bij een import op wordt gecontroleerd. Dit is ter bescherming van de integriteit van de database bij een import.</p>\r\n\r\n<h2>Importeren van uw data</h2>\r\n<p>Geëporteerde data kan uiteraard ook weer worden geïmporteerd. In de data uit de voortgang-sectie kunnen verwijzingen zitten naar gebruikers. Omdat gebruikers niet worden meegenomen in de export, moeten deze eerst worden aangemaakt. Verwijzingen naar niet-bestaande gebruikers worden na de import aangegeven en tevens vermeld in het informatie-veld van een taak. Er kan gekozen worden om de ontbrekende gebruikers aan te maken en de import opnieuw uit te voeren.</p>',1,1,0,NULL,NULL,NULL),(11,'/documentatie/adviseur','nl',NULL,0,NULL,'Documentatie - Adviseur','','','<p>RAVIB is ook inzetbaar voor adviseurs op het gebied van informatiebeveiliging. Zowel de adviseur als de klant maken daarvoor een eigen RAVIB-account aan voor hun eigen organisatie. De adviseur stuurt via de adviseursmodule een verzoek naar de klant om toegang te krijgen tot diens risicoanalyses. Indien dat verzoek door de klant wordt goedgekeurd, dan kan de adviseur via de adviseursmodule tijdelijk overschakelen naar de organisatie van de klant. De adviseur ziet dan de BIA, de actoren en de risicoanalyses van de klant. Zowel de adviseur als de klant kunnen deze toestemming weer intrekken.</p>\r\n\r\n<p>Het goedkeuren van een verzoek tot toegang kan alleen gebeuren door iemand met de Beheerder-rol. Een beheerder klikt daarvoor in de balk onderaan op de CMS-link. In het CMS kan via de Advisors-module aanvragen worden goedgekeurd en geweigerd en gegeven toestemmingen weer worden ingetrokken.</p>',1,1,0,NULL,NULL,NULL),(12,'/documentatie/gebruikersbeheer','nl',NULL,0,NULL,'Documentatie - Gebruikersbeheer','','','<p>Het is voor een gebruiker met de Beheerder-rol mogelijk om andere gebruikers binnen dezelfde organisatie te beheren. Een beheerder klikt daarvoor in de balk onderaan op de CMS-link.</p>\r\n\r\n<p>Via de Users-module kunnen accounts voor andere gebruikers worden aangemaakt, gewijzigd of verwijderd. De Access-module geeft een overzicht van de rechten van de verschillende gebruikers en de mogelijke rollen. De Controleur-rol is voor mensen die taken die zijn uitgedeeld in de Voortgang-module moeten kunnen goedkeuren, maar geen verdere toegang tot RAVIB willen of mogen hebben.</p>',1,1,0,NULL,NULL,NULL),(13,'/documentatie/belangen','nl',NULL,0,NULL,'Documentatie - Te beschermen belangen','','','<p>De informatie binnen de scope en de processen die door deze informatie worden ondersteund, dienen een of meerdere belangen. Het is goed om deze belangen duidelijk en volledig vast te leggen. Dit helpt namelijk bij het bepalen van de impact. Het helpt ook om binnen de organisatie het belang van informatiebeveiliging duidelijk te krijgen. Informatiebeveiliging is namelijk geen op zichzelf staand iets. Het ondersteunt namelijk het behalen van organisatiedoelen.</p>\r\n\r\n<p>Denk bij het bepalen van de te beschermen belangen in termen van bedrijfsdoelen en imago, maar ook aan belangen van eventuele derde partijen.</p>',1,1,0,NULL,NULL,NULL),(14,'/documentatie/scenarios','nl',NULL,0,NULL,'Documentatie - Scenario\'s','','','<p>Een groot incident is vaak niet het gevolg van een enkele kwetsbaarheid, maar een aaneenschakeling van incidenten. Zo begon de hack in de Rotterdamse haven met het binnenhalen van een met malware besmette software-update bij een vestiging in Oekraïne, omdat de leverancier van die software gehackt was. Via een internationaal intern netwerk (netwerksegmentering en interne firewalls ontbraken) kon de malware de systemen in de Rotterdamse haven bereiken en deze besmetten door het ontbreken van security updates.</p>\r\n\r\n<p>Bedenk in deze stap dus hoe meerdere dreigingen samen tot een groot incident kunnen leiden. Zo\'n scenario kan wat Hollywood-achtig aanvoelen, maar dat waren de beschrijvingen van de Diginotar-hack en de aanval op de Rotterdamse haven van te voren ook. Het gaat bij een scenario niet om of het waarschijnlijk is, maar of het technisch gezien mogelijk is.</p>\r\n\r\n<p>In de documentatie voor de dreigingsanalyse is geadviseerd om ook de dreigingen met een laag risico op te nemen. Dit zijn namelijk mogelijke opstapjes naar een groot incident. Denk dus ook over deze risico\'s na om te bepalen of deze onderdeel kunnen vormen van een scenario.</p>\r\n\r\n<p>Door op basis van de concreet vastgestelde dreigingen zelf een of meerdere scenario\'s te bedenken, kan de ernst van de afzonderlijke dreigingen en kwetsbaarheden beter begrijpbaar gemaakt worden. Dit maakt het makkelijker om aandacht te vragen voor de noodzaak van de mitigerende maatregelen en de eventueel daarvoor noodzakelijke investeringen.</p>',1,1,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reroute`
--

DROP TABLE IF EXISTS `reroute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reroute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original` varchar(100) NOT NULL,
  `replacement` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `description` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `risk_assess_sessions`
--

DROP TABLE IF EXISTS `risk_assess_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risk_assess_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_code` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `risk_assess_values`
--

DROP TABLE IF EXISTS `risk_assess_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risk_assess_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `risk_assess_session_id` int(10) unsigned NOT NULL,
  `chance` tinyint(3) unsigned NOT NULL,
  `impact` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `risk_assess_session_id` (`risk_assess_session_id`),
  CONSTRAINT `risk_assess_values_ibfk_1` FOREIGN KEY (`risk_assess_session_id`) REFERENCES `risk_assess_sessions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `non_admins` smallint(6) NOT NULL,
  `cms` tinyint(1) NOT NULL,
  `cms/access` tinyint(1) NOT NULL,
  `cms/action` tinyint(1) NOT NULL,
  `cms/advisor` tinyint(4) DEFAULT 0,
  `cms/analytics` tinyint(1) NOT NULL,
  `cms/apitest` tinyint(1) NOT NULL,
  `cms/file` tinyint(1) NOT NULL,
  `cms/language` tinyint(1) NOT NULL,
  `cms/measures` tinyint(4) DEFAULT 0,
  `cms/measures/categories` tinyint(4) DEFAULT 0,
  `cms/measures/export` tinyint(4) DEFAULT 0,
  `cms/menu` tinyint(1) NOT NULL,
  `cms/organisation` tinyint(1) NOT NULL,
  `cms/page` tinyint(1) NOT NULL,
  `cms/reroute` tinyint(4) DEFAULT 0,
  `cms/role` tinyint(1) NOT NULL,
  `cms/statistics` tinyint(4) DEFAULT 0,
  `cms/standards` tinyint(4) DEFAULT 0,
  `cms/settings` tinyint(1) NOT NULL,
  `cms/threats` tinyint(4) DEFAULT 0,
  `cms/threats/categories` tinyint(4) DEFAULT 0,
  `cms/threats/export` tinyint(4) DEFAULT 0,
  `cms/user` tinyint(1) NOT NULL,
  `cms/validate` tinyint(4) DEFAULT 0,
  `account` tinyint(4) DEFAULT 0,
  `actoren` tinyint(4) DEFAULT 0,
  `adviseur` tinyint(4) DEFAULT 0,
  `bia` tinyint(4) DEFAULT 0,
  `casus` tinyint(4) DEFAULT 0,
  `casus/belangen` tinyint(4) DEFAULT 0,
  `casus/dreigingen` tinyint(4) DEFAULT 0,
  `casus/maatregelen` tinyint(4) DEFAULT 0,
  `casus/rapportage` tinyint(4) DEFAULT 0,
  `casus/risico` tinyint(4) DEFAULT 0,
  `casus/scenarios` tinyint(4) DEFAULT 0,
  `casus/scope` tinyint(4) DEFAULT 0,
  `casus/voortgang` tinyint(4) DEFAULT 0,
  `casus/voortgang/export` tinyint(4) DEFAULT 0,
  `casus/voortgang/gereed` tinyint(4) DEFAULT 0,
  `casus/voortgang/rapportage` tinyint(4) DEFAULT 0,
  `dashboard` tinyint(4) DEFAULT 0,
  `data` tinyint(4) DEFAULT 0,
  `session` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--
-- ORDER BY:  `id`

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator',0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(2,'Beheerder',1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(3,'Gebruiker',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1),(4,'Adviseur',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1),(5,'Controleur',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(128) NOT NULL,
  `login_id` varchar(128) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `expire` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `bind_to_ip` tinyint(1) NOT NULL,
  `name` tinytext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `type` varchar(8) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--
-- ORDER BY:  `id`

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'admin_page_size','integer','25'),(2,'database_version','integer','3'),(3,'default_language','string','nl'),(8,'head_description','string','Gratis en open methode voor het uitvoeren van een risicoanalyse voor informatiebeveiliging op basis van ISO/IEC 27002, NEN 7510 of de BIO.'),(9,'head_keywords','string','risicoanalyse, informatiebeveiliging, cyber security, informatieveiligheid, dreigingen, ISO/IEC 27002, ISO/IEC 27001, NEN 7510, BIO, maatregelen, business impact analyse, gratis methodiek, open methodiek'),(10,'head_title','string','RAVIB'),(11,'hiawatha_cache_default_time','integer','3600'),(12,'hiawatha_cache_enabled','boolean','false'),(27,'secret_website_code','string',''),(28,'session_persistent','boolean','true'),(29,'session_timeout','integer','86400'),(30,'start_page','string','dashboard'),(33,'webmaster_email','string','root@localhost'),(45,'default_standard','integer','2'),(46,'validate_import_signature','boolean','true'),(47,'advisor_role','string','Adviseur');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standards`
--

DROP TABLE IF EXISTS `standards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standards`
--
-- ORDER BY:  `id`

LOCK TABLES `standards` WRITE;
/*!40000 ALTER TABLE `standards` DISABLE KEYS */;
INSERT INTO `standards` VALUES (1,'NEN-ISO/IEC 27002:2017',1),(2,'NEN-ISO/IEC 27002:2022',1);
/*!40000 ALTER TABLE `standards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `threat_categories`
--

DROP TABLE IF EXISTS `threat_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `threat_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `threat_categories`
--
-- ORDER BY:  `id`

LOCK TABLES `threat_categories` WRITE;
/*!40000 ALTER TABLE `threat_categories` DISABLE KEYS */;
INSERT INTO `threat_categories` VALUES (1,'Verantwoordelijkheid'),(2,'Wet- en regelgeving'),(3,'Continuïteit en betrouwbaarheid van systemen'),(4,'Incidentafhandeling'),(5,'Toegang tot informatie'),(6,'Uitwisselen en bewaren van informatie'),(8,'Menselijk handelen'),(9,'Fysieke beveiliging'),(10,'Bedrijfscontinuïteit');
/*!40000 ALTER TABLE `threat_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `threats`
--

DROP TABLE IF EXISTS `threats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `threats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL,
  `threat` text NOT NULL,
  `description` text NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `confidentiality` enum('p','s','-') NOT NULL,
  `integrity` enum('p','s','-') NOT NULL,
  `availability` enum('p','s','-') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `threats_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `threat_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `threats`
--
-- ORDER BY:  `id`

LOCK TABLES `threats` WRITE;
/*!40000 ALTER TABLE `threats` DISABLE KEYS */;
INSERT INTO `threats` VALUES (1,1,'Gebrek aan sturing op informatiebeveiliging vanuit de directie.','De directie stuurt niet op informatiebeveiliging. Verantwoordelijkheden richting lijnmanagers zijn niet belegd. Een informatiebeveiligingsbeleid en/of ISMS ontbreekt.',1,'p','p','p'),(2,2,'Lijnmanagers nemen hun verantwoordelijkheid voor informatiebeveiliging niet.','Lijnmanagers zorgen er onvoldoende voor dat informatiebeveiliging binnen hun afdeling op de juiste manier wordt ingevuld. Het eigenaarschap van informatiesystemen is niet goed belegd. Beveiliging vormt geen vast onderdeel van projecten.',1,'p','p','p'),(3,4,'Medewerkers handelen onvoldoende naar hetgeen van hen verwacht wordt.','Het ontbreekt de medewerkers aan bewustzijn op het gebied van informatiebeveiliging en voelen onvoldoende noodzaak daaraan bij te dragen.',1,'p','p','p'),(7,36,'Wetgeving over informatie in de cloud.','Door wetgeving in sommige landen kan de overheid van zo\'n land inzage krijgen in informatie welke in de cloud ligt opgeslagen.',2,'p','',''),(8,37,'Buitenlandse wetgeving bij het bezoeken van een land.','Door wetgeving in sommige landen kan de overheid inzage eisen in de gegevens op meegenomen systemen bij een bezoek aan dat land.',2,'p','',''),(9,38,'Wetgeving over het gebruik van cryptografie.','Door wetgeving in sommige landen kan de overheid een kopie van cryptografische sleutels opeisen.',2,'p','',''),(11,6,'Toegang tot informatie wordt geblokkeerd.','Informatie op een systeem is ontoegankelijk gemaakt, doordat malware (ransomware, wipers) of een aanvaller deze informatie heeft versleuteld of verwijderd.',3,'','','p'),(12,7,'Netwerkdiensten raken overbelast.','Een netwerkdienst is verminderd beschikbaar door een moedwillige aanval (DoS) of door onvoorziene toename van de hoeveelheid verzoeken of van de benodigde resources om een verzoek af te handelen. Verzoeken kunnen komen vanuit gebruikers, maar ook vanuit andere systemen.',3,'','','p'),(13,39,'Incidenten worden niet tijdig opgepakt.','De gevolgen van incidenten worden hierdoor onnodig groot. Binnen het bedrijf is er onvoldoende netwerkmonitoring en is er geen centraal meldpunt voor beveiligingsincidenten.',4,'p','s',''),(14,40,'Informatie voor het aanpakken van incidenten ontbreekt.','Systeembeheerders hebben onvoldoende technische informatie over het probleem om het te kunnen oplossen. Een actieplan ontbreekt waardoor het incident onnodig lang blijft duren.',4,'p','p','p'),(15,41,'Herhaling van incidenten.','Veroorzakers van incidenten worden niet aangesproken op hun handelen. Managers hebben onvoldoende zicht op herhalende incidenten, waardoor zij daar niet op sturen.',4,'p','s','p'),(16,14,'Systemen worden niet gebruikt waarvoor ze bedoeld zijn.','Het ontbreken van een beleid op bijvoorbeeld het internetgebruik, vergroot de kans op misbruik.',8,'s','','p'),(17,15,'Wegnemen van bedrijfsmiddelen.','Door onvoldoende controle op de uitgifte en onjuiste inventarisatie van bedrijfsmiddelen bestaat de kans dat diefstal niet of te laat wordt opgemerkt.',8,'s','','p'),(18,16,'Beleid wordt niet gevolgd door ontbreken van sancties.','Door het ontbreken van sancties op het overtreden van regels bestaat de kans dat medewerkers de beleidsmaatregelen niet serieus nemen.',8,'p','s',''),(19,17,'Toelaten van externen in het pand of op het netwerk.','Het toelaten van externen, zoals leveranciers en projectpartners, kunnen gevolgen hebben voor de vertrouwelijkheid van de informatie die binnen het pand of via het netwerk beschikbaar is.',8,'p','',''),(20,19,'Misbruik van andermans identiteit.','Door onvoldoende (mogelijkheid op) controle op een identiteit, kan ongeautoriseerde toegang verkregen worden tot vertrouwelijke informatie. Denk hierbij ook aan social engineering, zoals phishing en CEO-fraude.',8,'p','p',''),(21,21,'Onterecht hebben van rechten.','Door een ontbrekend, onjuist of onduidelijk proces voor het uitdelen en innemen van rechten, kan een persoon onbedoeld meer rechten hebben. Deze rechten kunnen door deze persoon zelf of door anderen (bijv. via malware) misbruikt worden.',8,'p','p',''),(22,20,'Misbruik van speciale bevoegdheden.','Door onvoldoende controle op medewerkers met bijzondere rechten, zoals systeembeheerders, bestaat de kans op ongeautoriseerde toegang tot gevoelige informatie.',8,'p','p',''),(23,22,'Slecht wachtwoordgebruik.','Het ontbreken van een wachtwoordbeleid en bewustzijn bij medewerkers kan leiden tot het gebruik van zwakke wachtwoorden, het opschrijven van wachtwoorden of het gebruik van hetzelfde wachtwoord voor meerdere systemen.',5,'p','p',''),(24,23,'Onbeheerd achterlaten van werkplekken.','Door het ontbreken van een clear-desk en/of clear-screen policy kan toegang verkregen worden tot gevoelige informatie.',5,'p','s',''),(25,24,'Onduidelijkheid over classificatie en bevoegdheden.','Door onduidelijkheid over vertrouwelijkheid van informatie van informatie en bevoegdheid van personen bestaat de kans op ongeautoriseerde toegang tot gevoelige informatie.',5,'p','s',''),(26,25,'Informatie op systemen bij reparatie of verwijdering.','Gevoelige informatie kan lekken indien opslagmedia of systemen welke opslagmedia bevatten worden weggegooid of ter reparatie aan derden worden aangeboden.',5,'p','',''),(27,35,'Misbruik van cryptografische sleutels en/of gebruik van zwakke algoritmen.','Door een onjuist of ontbrekend sleutelbeheer bestaat de kans op misbruik van cryptografische sleutels. Het gebruik van zwakke cryptografische algoritmen biedt schijnveiligheid.',6,'p','p',''),(28,26,'Misbruik van kwetsbaarheden in applicaties of hardware.','Kwetsbaarheden in applicaties of hardware worden misbruikt (exploits) om ongeautoriseerde toegang te krijgen tot een applicatie en de daarin opgeslagen informatie.',5,'p','p',''),(29,27,'Misbruiken van zwakheden in netwerkbeveiliging.','Zwakheden in de beveiliging van het (draadloze) netwerk worden misbruikt om toegang te krijgen tot dit netwerk.',5,'p','p',''),(30,29,'Informatie buiten de beschermde omgeving.','Informatie die voor toegestaan gebruik meegenomen wordt naar bijvoorbeeld buiten het kantoor wordt niet meer op de juiste wijze beschermd. Denkbij ook aan Bring Your Own Device (BYOD).',5,'p','',''),(31,30,'Afluisterapparatuur.','Door middel van keyloggers of netwerktaps wordt gevoelige informatie achterhaald.',5,'p','',''),(32,31,'Onveilig versturen van gevoelige informatie.','Inbreuk op vertrouwelijkheid van informatie door onversleuteld versturen van informatie.',6,'p','s',''),(33,32,'Versturen van gevoelige informatie naar onjuiste persoon.','Inbreuk op vertrouwelijkheid van informatie door het onvoldoende controleren van ontvanger.',6,'p','',''),(35,33,'Informatieverlies door verlopen van houdbaarheid van opslagwijze.','Informatie gaat verloren door onleesbaar geraken van medium of gedateerd raken van bestandsformaat.',6,'','','p'),(36,34,'Foutieve informatie.','Ongewenste handelingen als gevolg van foutieve bedrijfsinformatie of het toegestuurd krijgen van foutieve informatie. Dit kan zijn als gevolg van moedwillig handelen of van een vergissing.',6,'','p',''),(37,18,'Verlies van mobiele apparatuur en opslagmedia.','Door het verlies van mobiele apparatuur en opslagmedia bestaat de kans op inbreuk op de vertrouwelijkheid van gevoelige informatie.',8,'p','','s'),(38,8,'Aanvallen via systemen die niet in eigen beheer zijn.','Door onvoldoende grip op de beveiliging van prive- en thuisapparatuur en andere apparatuur van derden, bestaat de kans op bijvoorbeeld besmetting met malware.',3,'','','p'),(39,11,'Uitval van systemen door softwarefouten.','Fouten in software kunnen leiden tot systeemcrashes of het corrupt raken van de in het systeem opgeslagen informatie.',3,'','p','p'),(40,10,'Uitval van systemen door configuratiefouten.','Onjuiste configuratie van een applicatie kan leiden tot een verkeerde verwerking van informatie.',3,'','p','s'),(41,9,'Uitval van systemen door hardwarefouten.','Hardware van onvoldoende kwaliteit kan leiden tot uitval van systemen.',3,'','','p'),(42,13,'Gebruikersfouten.','Onvoldoende kennis of te weinig controle op andermans werk vergroot de kans op menselijke fouten. Gebruikersinterfaces die niet zijn afgestemd op het gebruikersniveau verhogen de kans op fouten.',8,'','p','s'),(43,50,'Software wordt niet meer ondersteund door de uitgever.','Voor software die niet meer ondersteund wordt worden geen securitypatches meer uitgegeven. Denk ook aan Excel- en Access-applicaties.',10,'','','p'),(44,42,'Ongeautoriseerde fysieke toegang.','Het ontbreken van toegangspasjes, zicht op ingangen en bewustzijn bij medewerkers vergroot de kans op ongeautoriseerde fysieke toegang.',9,'p','',''),(45,43,'Brand.','Het ontbreken van brandmelders en brandblusapparatuur vergroten de gevolgen van een brand.',9,'','','p'),(46,45,'Overstroming en wateroverlast.','Overstroming en wateroverlast kunnen zorgen voor schade aan computers en andere bedrijfsmiddelen.',9,'','','p'),(47,46,'Verontreiniging van de omgeving.','Verontreininging van de omgeving kan ertoe leiden dat de organisatie (tijdelijk) niet meer kan werken.',9,'','','p'),(48,44,'Explosie.','Explosies kunnen leiden tot schade aan het gebouw en apparatuur en slachtoffers.',9,'','','p'),(49,47,'Uitval van facilitaire middelen (gas, water, electra, airco).','Uitval van facilitaire middelen kan tot gevolg hebben dat een of meerdere bedrijfsonderdelen hun werk niet meer kunnen doen.',9,'','','p'),(50,48,'Vandalisme.','Schade aan of vernieling van bedrijfseigendommen als gevolg van een ongerichte actie, zoals vandalisme of knaagdieren.',9,'','','p'),(52,49,'Niet beschikbaar zijn van diensten van derden.','Het niet meer beschikbaar zijn van diensten van derden door uitval van systemen, faillissement, ongeplande contractbeëindiging of onacceptabele wijziging in de dienstverlening (bijvoorbeeld door bedrijfsovername).',10,'','','p'),(53,51,'Kwijtraken van belangrijke kennis bij niet beschikbaar zijn van medewerkers.','Medewerkers die het bedrijf verlaten of door een ongeval voor lange tijd niet inzetbaar zijn, bezitten kennis die daardoor niet meer beschikbaar is.',10,'','','p'),(56,12,'Fouten als gevolg van wijzigingen in andere systemen.','In een systeem ontstaan fouten als gevolg van wijzigingen in gekoppelde systemen.',3,'','p','p'),(57,5,'Onvoldoende aandacht voor beveiliging bij softwareontwikkeling.','Onvoldoende aandacht voor beveiliging bij het zelf of laten ontwikkelen van software leidt tot inbreuk op de informatiebeveiliging.',3,'p','p','p'),(58,28,'Onvoldoende aandacht voor beveiliging bij uitbesteding van werkzaamheden.','Doordat externe partijen / leveranciers hun informatiebeveiliging niet op orde hebben, kunnen inbreuken ontstaan op de informatie waar zij toegang tot hebben.',5,'p','',''),(59,3,'Onvoldoende aandacht voor beveiliging binnen projecten.','Binnen projecten (exclusief softwareontwikkeling) is onvoldoende aandacht voor beveiliging. Dit heeft negatieve gevolgen voor nieuwe systemen en processen die binnen de organisatie worden geïntroduceerd.',1,'p','s','p');
/*!40000 ALTER TABLE `threats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password` tinytext NOT NULL,
  `status` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `authenticator_secret` varchar(16) DEFAULT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `crypto_key` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `organisation_id` (`organisation_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--
-- ORDER BY:  `id`

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','none',1,NULL,'Administrator','root@localhost',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-16 17:04:12
