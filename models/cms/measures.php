<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_measures_model extends Banshee\model {
		public function get_standard($standard) {
			return $this->borrow("cms/standards")->get_item($standard);
		}

		public function count_measures($standard) {
			$query = "select count(*) as count from measures where standard_id=%d";

			if (($result = $this->db->execute($query, $standard)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_measures($standard, $offset, $limit) {
			$query = "select m.*, (select count(*) from mitigation where measure_id=m.id) as links from measures m ".
			         "where standard_id=%d order by id limit %d,%d";

			return $this->db->execute($query, $standard, $offset, $limit);
		}

		public function get_measure($measure_id) {
			if (($result = $this->db->entry("measures", $measure_id)) === false) {
				return false;
			}

			$query = "select * from mitigation where measure_id=%d";
			if (($links = $this->db->execute($query, $measure_id)) === false) {
				return false;
			}

			$result["threat_links"] = array();
			foreach ($links as $link) {
				array_push($result["threat_links"], $link["threat_id"]);
			}
			
			return $result;
		}

		public function get_categories($standard) {
			$query = "select * from measure_categories where standard_id=%d order by number";

			if (($categories = $this->db->execute($query, $standard)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["number"]] = $category;
			}

			return $result;
		}

		public function get_threats() {
			$query = "select * from threats order by number";

			return $this->db->execute($query);
		}

		public function save_oke($measure) {
			$result = true;

			if ((trim($measure["number"]) == "") || (trim($measure["name"]) == "")) {
				$this->view->add_message("Please, fill in the number and name.");
				$result = false;
			}

			return $result;
		}

		private function save_threat_links($threat, $measure_id) {
			if (is_array($threat["threat_links"] ?? null) == false) {
				return true;
			}

			foreach ($threat["threat_links"] as $threat_id) {
				$data = array(
					"measure_id" => $measure_id,
					"threat_id"      => $threat_id);
				if ($this->db->insert("mitigation", $data) === false) {
					return false;
				}
			}

			return true;
		}

		public function create_measure($measure, $standard) {
			$keys = array("id", "standard_id", "number", "name", "reduce");

			$measure["id"] = null;
			$measure["standard_id"] = $standard;

			if ($this->db->query("begin") === false) {	
				return false;
			}

			if ($this->db->insert("measures", $measure, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}
			$measure_id = $this->db->last_insert_id;

			if ($this->save_threat_links($measure, $measure_id) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function update_measure($measure, $standard) {
			$keys = array("number", "name", "reduce");

			if ($this->db->query("begin") === false) {	
				return false;
			}

			if ($this->db->update("measures", $measure["id"], $measure, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			$query = "delete from mitigation where measure_id=%d";
			if ($this->db->query($query, $measure["id"], $standard) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->save_threat_links($measure, $measure["id"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($measure) {
			return true;
		}

		public function delete_measure($measure_id) {
			$queries = array(
				array("delete from mitigation where measure_id=%d", $measure_id),
				array("delete from overruled where measure_id=%d", $measure_id),
				array("delete from measures where id=%d", $measure_id));

			return $this->db->transaction($queries);
		}
	}
?>
