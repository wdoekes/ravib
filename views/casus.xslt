<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:if test="@archive='yes'">
<h3>Archief</h3>
</xsl:if>
<form action="/{/output/page}" method="post">
<table class="table table-condensed table-striped">
<thead>
<tr>
<th>Onderwerp</th>
<th>Standaard</th>
<th>Datum</th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="case">
<tr>
<td><xsl:if test="organisation!=''"><xsl:value-of select="organisation" /> :: </xsl:if><xsl:value-of select="name" /></td>
<td><xsl:value-of select="standard" /></td>
<td><xsl:value-of select="date" /></td>
<td><a href="/{start}/{@id}" id="start" class="btn btn-xs btn-primary">Start</a> <a href="/{/output/page}/{@id}" class="btn btn-xs btn-default">Bewerk</a></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<xsl:if test="@archive='no'">
<a href="/{/output/page}/nieuw" class="btn btn-default">Nieuwe casus</a>
<xsl:if test="@archived>0">
<a href="/{/output/page}/archief" class="btn btn-default">Toon archief</a>
</xsl:if>
<a href="/dashboard" class="btn btn-default">Terug</a>
</xsl:if>
<xsl:if test="@archive='yes'">
<a href="/{/output/page}" class="btn btn-default">Archief verlaten</a>
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="case/@id">
<input type="hidden" name="id" value="{case/@id}" />
</xsl:if>

<div class="row">
<div class="col-md-6">
<label for="name">Organisatie(onderdeel):</label>
<input type="text" id="organisation" name="organisation" value="{case/organisation}" class="form-control" />
<label for="name">Naam van de casus (tip: scope in &#233;&#233;n woord):</label>
<input type="text" id="name" name="name" value="{case/name}" class="form-control" />
<label for="standard_id">Standaard:</label>
<select id="standard_id" name="standard_id" class="form-control">
<xsl:for-each select="standards/standard">
<option value="{@id}"><xsl:if test="@id=../../case/standard_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="date">Datum:</label>
<input type="text" id="date" name="date" value="{case/date}" class="form-control datepicker" />
<label for="scope">Scope. De risicoanalyse heeft zich beperkt tot ...</label>
<textarea id="scope" name="scope" class="form-control"><xsl:value-of select="case/scope" /></textarea>
<label for="logo">URL van logo voor in rapportage:</label>
<input type="text" id="logo" name="logo" value="{case/logo}" class="form-control" />
<xsl:if test="case/@id">
<label for="archived">Gearchiveerd:</label>
<input type="checkbox" id="archived" name="archived"><xsl:if test="case/archived='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
</xsl:if>
</div>

<div class="col-md-6">
<h3>Invulling van de impact</h3>
<p>Vul hier het verwachte schadebedrag en/of de verwachte imagoschade in.</p>
<xsl:for-each select="impact/value">
<label for="impact_{position()}"><xsl:value-of select="@label" />:</label>
<input type="text" id="impact_{position()}" name="impact[]" value="{.}" class="form-control" />
</xsl:for-each>
</div>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="Casus opslaan" class="btn btn-default" />
<xsl:if test="case/archived='no'">
<a href="/{/output/page}" class="btn btn-default">Afbreken</a>
</xsl:if>
<xsl:if test="case/archived='yes'">
<a href="/{/output/page}/archief" class="btn btn-default">Afbreken</a>
</xsl:if>
<xsl:if test="case/@id">
<input type="submit" name="submit_button" value="Casus verwijderen" class="btn btn-default" onClick="javascript:return confirm('VERWIJDEREN: Weet u het zeker?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/casus.png" class="title_icon" />
<h1>Casussen</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
