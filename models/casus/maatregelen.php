<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_maatregelen_model extends ravib_model {
		private function sort_threats($threat1, $threat2) {
			return strcmp($threat1["threat"], $threat2["threat"]);
		}

		public function get_case_threats($case_id) {
			$query = "select t.*, (select count(*) from case_threat_measure s, cases c, measures m ".
			         "where s.case_threat_id=t.id and t.case_id=c.id and s.measure_id=m.id ".
			         "and c.standard_id=m.standard_id) as measures from case_threats t where case_id=%d";

			if (($threats = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			foreach (array_keys($threats) as $key) {
				$this->decrypt($threats[$key], "threat", "action", "current", "argumentation");
			}

			uasort($threats, array($this, "sort_threats"));

			return $threats;
		}

		public function get_case_threat($threat_id, $case_id) {
			return $this->borrow("casus/dreigingen")->get_case_threat($threat_id, $case_id);
		}

		public function get_selected_measures($threat_id, $case_id) {
			$query = "select s.* from case_threat_measure s, case_threats t, cases c, measures m ".
			         "where s.case_threat_id=t.id and t.id=%d and t.case_id=c.id and c.id=%d ".
			         "and s.measure_id=m.id and m.standard_id=c.standard_id";

			if (($selected = $this->db->execute($query, $threat_id, $case_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($selected as $item) {
				array_push($result, $item["measure_id"]);
			}

			return $result;
		}

		public function get_measures_standard($standard_id) {
			$query = "select * from measure_categories where standard_id=%d order by number";
			if (($result = $this->db->execute($query, $standard_id)) === false) {
				return false;
			}

			$categories = array();
			foreach ($result as $item) {
				$categories[$item["number"]] = $item;
			}

			$query = "select * from measures where standard_id=%d order by id";
			if (($measures = $this->db->execute($query, $standard_id, ".")) === false) {
				return false;
			}

			$category = 0;
			foreach ($measures as $m => $measure) {
				list($number) = explode(".", $measure["number"], 2);
				if ($number != $category) {
					$category = $number;
					$measures[$m]["category"] = $categories[$category];
				}
			}

			return $measures;
		}

		public function get_measures_threat($threat_id) {
			$query = "select * from measures m, mitigation t ".
			         "where m.id=t.measure_id and t.threat_id=%d";

			return $this->db->execute($query, $threat_id);
		}

		public function get_threats() {
			$query = "select * from threats order by number";

			return $this->db->execute($query);
		}

		public function effective_measure($measure_reduce, $threat_handle) {
			/* x = measure_reduce: kans|impact|kans & impact
			 * y = threat_handle:  beheersen|ontwijken|verweren|accepteren
			 */
			$effective = array(
				array(YES, YES, YES),
				array(YES,  NO, YES),
				array( NO, YES, YES),
				array( NO,  NO,  NO));

			return $effective[$threat_handle - 1][$measure_reduce];
		}

		public function save_measures($measures, $case) {
			if ($this->get_case_threat($measures["case_threat_id"], $case["id"]) == false) {
				return false;
			}

			$this->db->query("begin");

			$query = "delete from case_threat_measure where case_threat_id=%d and measure_id in ".
			         "(select id from measures where standard_id=%d)";
			if ($this->db->query($query, $measures["case_threat_id"], $case["standard_id"]) === false) {
				$this->db->query("rollback");
				return false;
			}

			if (is_array($measures["selected"] ?? null)) {
				$data = array("case_threat_id" => $measures["case_threat_id"]);
				foreach ($measures["selected"] as $measure_id) {
					$data["measure_id"] = $measure_id;
					if ($this->db->insert("case_threat_measure", $data) === false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
