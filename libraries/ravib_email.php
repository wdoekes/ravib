<?php
	class ravib_email extends \Banshee\Protocol\email {
		protected $content_type = "text/html";
		private $footers = array();

		/* Constructor
		 *
		 * INPUT:  string subject[, string e-mail][, string name]
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($subject, $from_address = null, $from_name = null) {
			$this->add_footer("Deze e-mail is automatisch gegenereerd door RAVIB en daarom niet ondertekend.");

			parent::__construct($subject, $from_address, $from_name);
		}

		/* Add e-mail footer
		 *
		 * INPUT:  string footer
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function add_footer($str) {
			array_push($this->footers, $str);
		}

		public function set_message_fields($data = null) {
			parent::set_message_fields($data);

			$footer = implode("<span style=\"margin:0 10px\">|</span>", $this->footers);
			$cid = $this->add_image("images/ravib_small.png");

			$data = array(
				"FOOTER" => $footer,
				"LOGO"   => $cid);
			foreach ($data as $key => $value) {
				$key = sprintf($this->field_format, $key);
				$this->message_fields[$key] = $value;
			}
		}

		/* Set newsletter content
		 *
		 * INPUT:  string content
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function message($content) {
			$message = file_get_contents("../extra/ravib_email.txt");
			$message = str_replace("[MESSAGE]", $content, $message);

			parent::message($message);
		}
	}
?>
