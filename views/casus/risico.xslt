<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Assess template
//
//-->
<xsl:template match="assess">
<table class="matrix">
<tr><td></td><td></td><td colspan="{count(matrix/row)-1}">Impact</td></tr>
<xsl:for-each select="matrix/row">
<tr>
	<xsl:if test="position()=1"><td></td></xsl:if>
	<xsl:if test="position()=2"><td rowspan="{count(../row)-1}" class="chance">Kans</td></xsl:if>
	<xsl:for-each select="cell">
		<td><xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if><xsl:value-of select="." /></td>
	</xsl:for-each>
</tr>
</xsl:for-each>
</table>

<form action="/{/output/page}" method="post">
<div class="btn-group">
<a href="/{/output/page}" class="btn btn-default">Ververs matrix</a>
<input type="submit" name="submit_button" value="Leeg matrix" class="btn btn-default" />
</div>
<div class="btn-group right">
<input type="submit" name="submit_button" value="Nieuwe toegangscode" class="btn btn-default" onClick="javascript:return confirm('Weet u het zeker?')" />
</div>
</form>

<div id="help">
<p>Laat deelnemers van de risicoanalyse-sessie de volgende URL bezoeken:</p>
<span>https://<xsl:value-of select="/output/website_url" />/risico</span>
<p>De benodigde toegangscode is:</p>
<span><xsl:value-of select="access_code" /></span>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Risico-inschatting</h1>
<xsl:apply-templates select="assess" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
