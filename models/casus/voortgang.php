<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_voortgang_model extends ravib_model {
		public function get_measure($measure_id) {
			return $this->db->entry("measures", $measure_id);
		}

		public function get_measure_categories($standard_id) {
			$query = "select * from measure_categories where standard_id=%d order by number";

			if (($categories = $this->db->execute($query, $standard_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["number"]] = $category["name"];
			}

			return $result;
		}

		public function get_case_measures($case_id) {
			$query = "select distinct m.*, UNIX_TIMESTAMP(p.deadline) as deadline, u.fullname as person, u.email, ".
			                "p.info, p.done, p.hours_planned, p.hours_invested ".
			         "from case_threats t, case_threat_measure l, cases c, measures m ".
			         "left join case_progress p on p.case_id=%d and p.measure_id=m.id ".
			         "left join users u on u.id=p.executor_id ".
			         "where t.case_id=c.id and c.id=%d and t.id=l.case_threat_id and l.measure_id=m.id ".
			         "and m.standard_id=c.standard_id and t.handle!=%d and t.handle!=%d";

			if (($measures = $this->db->execute($query, $case_id, $case_id, 0, THREAT_ACCEPT)) === false) {
				return false;
			}

			foreach (array_keys($measures) as $key) {
				$this->decrypt($measures[$key], "info");
			}

			/* Get threats
			 */
			$query = "select t.id, t.threat, t.chance, t.impact, t.handle ".
			         "from case_threats t, case_threat_measure m ".
			         "where t.id=m.case_threat_id and m.measure_id=%d and t.case_id=%d";
			foreach ($measures as $m => $measure) {
				if (($threats = $this->db->execute($query, $measure["id"], $case_id)) === false) {
					return false;
				}

				unset($measures[$m]["case_id"]);

				$highest_risk = -1;
				foreach ($threats as $t => $threat) {
					if (($threat["chance"] == 0) || ($threat["impact"] == 0) || ($threat["handle"] == 0)) {
						continue;
					}

					$risk = $this->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
					if ($risk >= $highest_risk) {
						$highest_risk = $risk;
					}
				}

				$measures[$m]["risk_value"] = $highest_risk;
				if ($highest_risk > -1) {
					$measures[$m]["urgency"] = $this->risk_matrix_labels[$highest_risk];
					$measures[$m]["urgency_level"] = $highest_risk;
				}

				$measures[$m]["done"] = is_true($measure["done"]);
			}

			return $measures;
		}

		public function get_case_threats($measure_id, $case_id) {
			$query = "select threat, handle, action, current from case_threats t, case_threat_measure m ".
			         "where t.case_id=%d and t.id=m.case_threat_id and m.measure_id=%d";

			if (($threats = $this->db->execute($query, $case_id, $measure_id)) === false) {
				return false;
			}

			foreach (array_keys($threats) as $key) {
				$this->decrypt($threats[$key], "threat", "current", "action");
			}

			return $threats;
		}

		public function sort_measures($a, $b, $order = null, $loop = 3) {
			if ($loop-- <= 0) {
				return 0;
			}

			if ($order == null) {
				$order = $_SESSION["progress_order"];
			}

			switch ($order) {
				case "deadline":
					if ($a["deadline"] == null) {
						$a["deadline"] = "9999-12-31";
					}
					if ($b["deadline"] == null) {
						$b["deadline"] = "9999-12-31";
					}
					if (($result = strcmp($a["deadline"], $b["deadline"])) == 0) {
						$result = $this->sort_measures($a, $b, "urgency", $loop);
					}
					break;
				case "person":
					if ($a["person"] == null) {
						$a["person"] = "zzzzzzzz";
					}
					if ($b["person"] == null) {
						$b["person"] = "zzzzzzzz";
					}
					if (($result = strcmp($a["person"], $b["person"])) == 0) {
						$result = $this->sort_measures($a, $b, "deadline", $loop);
					}
					break;
				case "urgency":
					if (($result = strcmp($b["risk_value"], $a["risk_value"])) == 0) {
						$result = $this->sort_measures($a, $b, "deadline", $loop);
					}
					break;
				case "done":
					if (($result = strcmp($a["done"], $b["done"])) == 0) {
						$result = $this->sort_measures($a, $b, "urgency", $loop);
					}
					break;
				default:
					$result = version_compare($a["number"], $b["number"]);
			}

			return $result;
		}

		public function get_people() {
			$query = "select id, fullname, email from users where organisation_id=%d";

			return $this->db->execute($query, $this->organisation_id);
		}

		public function get_person($id) {
			$query = "select id, fullname, email from users where id=%d and organisation_id=%d";

			if (($users = $this->db->execute($query, $id, $this->organisation_id)) == false) {
				return false;
			}

			return $users[0];
		}

		public function get_progress($measure_id, $case_id) {
			$query = "select * from case_progress where case_id=%d and measure_id=%d";
			if (($result = $this->db->execute($query, $case_id, $measure_id)) === false) {
				return false;
			}

			if (count($result) == false) {
				return array("measure_id" => $measure_id);
			} else {
				$this->decrypt($result[0], "info");
				return $result[0];
			}
		}

		public function progress_oke($progress, $case_id) {
			$result = true;

			if ($progress["executor_id"] != 0) {
				if ($this->get_person($progress["executor_id"]) == false) {
					$this->view->add_message("Onbekende persoon.");
					$result = false;
				}

				if ($progress["deadline"] == null) {
					$this->view->add_message("Geef een deadline op.");
					$result = false;
				}

				if ($progress["executor_id"] == $progress["reviewer_id"]) {
					$this->view->add_message("De persoon die de taak uitvoerd kan niet de controleur zijn.");
					$result = false;
				}
			} else if ($progress["deadline"] != "") {
				$this->view->add_message("Wijs de taak aan iemand toe.");
				$result = false;
			}


			return $result;
		}

		public function save_progress($progress, $case_id) {
			$query = "select count(*) as count from case_threats t, case_threat_measure m ".
			         "where t.case_id=%d and t.id=m.case_threat_id and measure_id=%d";
			if (($result = $this->db->execute($query, $case_id, $progress["measure_id"])) === false) {
				return false;
			}
			if ($result[0]["count"] == 0) {
				return true;
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			$query = "delete from case_progress where case_id=%d and measure_id=%d";
			if ($this->db->query($query, $case_id, $progress["measure_id"]) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($progress["deadline"] == "") {
				$progress["deadline"] = null;
			}

			$progress["info"] = trim($progress["info"]);

			if (($progress["executor_id"] == 0) && ($progress["reviewer_id"] == 0) && ($progress["deadline"] == null) && ($progress["info"] == "") && (($progress["done"] ?? null) == null) && ($progress["hours_planned"] == 0) && ($progress["hours_invested"] == 0)) {
				$query = "delete from case_progress where case_id=%d and measure_id=%d";
				if ($this->db->query($query, $case_id, $progress["measure_id"]) === false) {
					$this->db->query("rollback");
					return false;
				}
			} else {
				if ($progress["executor_id"] == 0) {
					$progress["executor_id"] = null;
				}

				if ($progress["reviewer_id"] == 0) {
					$progress["reviewer_id"] = null;
				}

				if (empty($progress["hours_planned"])) {
					$progress["hours_planned"] = 0;
				}

				if (empty($progress["hours_invested"])) {
					$progress["hours_invested"] = 0;
				}

				$this->encrypt($progress, "info");

				$data = array(
					"case_id"        => $case_id,
					"executor_id"    => $progress["executor_id"],
					"reviewer_id"    => $progress["reviewer_id"],
					"measure_id"     => $progress["measure_id"],
					"deadline"       => $progress["deadline"],
					"info"           => $progress["info"],
					"done"           => is_true($progress["done"] ?? null) ? YES : NO,
					"hours_planned"  => $progress["hours_planned"],
					"hours_invested" => $progress["hours_invested"]);
				if ($this->db->insert("case_progress", $data) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function get_signature($data) {
			return hash_hmac("sha256", json_encode($data), $this->settings->secret_website_code);
		}

		public function send_notifications($progress, $case_id) {
			if ($progress["executor_id"] == 0) {
				return true;
			}

			if (($case = $this->get_case($case_id)) == false) {
				return false;
			} else if (($executor = $this->get_person($progress["executor_id"])) == false) {
				return false;
			}

			if (($reviewer = $this->get_person($progress["reviewer_id"])) == false) {
				$reviewer = array("name" => "-");
			}

			if (($measure = $this->db->entry("measures", $progress["measure_id"])) == false) {
				return false;
			}

			if (($message = file_get_contents("../extra/taak_toegewezen.txt")) === false) {
				$this->view->add_system_warning("Can't load message template.\n");
				return false;
			}

			/* Send actor e-mail
			 */
			$replace = array(
				"NAME"        => $executor["fullname"] ?? "",
				"CASE"        => $case["name"] ?? "",
				"INFORMATION" => $progress["info"] ?? "",
				"MEASURE"     => $measure["number"]." ".$measure["name"],
				"REVIEWER"    => $reviewer["fullname"] ?? "");

			$mail = new ravib_email("Taak ".$measure["number"]." inzake ".$case["name"], $this->settings->webmaster_email, "RAVIB");
			$mail->set_message_fields($replace);
			$mail->message($message);
			$mail->send($executor["email"], $executor["name"] ?? null);

			/* Send reviewer e-mail
			 */
			if ($progress["reviewer_id"] == 0) {
				return true;
			}

			if (($message = file_get_contents("../extra/taak_controle.txt")) === false) {
				exit("Can't load message template.\n");
			}

			$data = array(
				"case_id"    => $case_id,
				"measure_id" => $progress["measure_id"]);
			$data["signature"] = $this->get_signature($data);
			$code = rtrim(strtr(base64_encode(json_encode($data)), "+/", "-_"), "=");

			$link = "https://".$_SERVER["HTTP_HOST"]."/casus/voortgang/gereed/".$code;

			$replace = array(
				"NAME"        => $reviewer["fullname"],
				"CASE"        => $case["name"],
				"INFORMATION" => $progress["info"],
				"MEASURE"     => $measure["number"]." ".$measure["name"],
				"EXECUTOR"    => $executor["fullname"],
				"LINK"        => $link);

			$mail = new ravib_email("Controle op taak ".$measure["number"]." inzake ".$case["name"], "no-reply@ravib.nl", "RAVIB");
			$mail->set_message_fields($replace);
			$mail->message($message);
			$mail->send($reviewer["email"], $reviewer["name"]);

			return true;
		}
	}
?>
