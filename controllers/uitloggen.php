<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class uitloggen_controller extends Banshee\controller {
		public function execute() {
			$attr = array("url" => $this->settings->start_page);

			if ($this->user->logged_in) {
				header("Status: 401");
				$this->user->logout();

				$this->view->add_tag("result", "U bent nu uitgelogd.", $attr);
			} else {
				$this->view->add_tag("result", "U bent niet ingelogd.", $attr);
			}
		}
	}
?>
