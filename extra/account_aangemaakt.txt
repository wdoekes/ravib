<p>Hallo [FULLNAME],</p>
<p>Voor u is een account aangemaakt voor de <a href="[PROTOCOL]://[HOSTNAME]/">RAVIB website</a>. Gebruik deze gegevens om in te loggen:</p>
<p>Gebruikersnaam: [USERNAME]<br />Wachtwoord: [PASSWORD]</p>
