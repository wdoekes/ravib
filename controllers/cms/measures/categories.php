<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_measures_categories_controller extends Banshee\tablemanager_controller {
		protected $name = "Measure category";
		protected $back = "cms/measures";
		protected $icon = "measure_categories.png";
		protected $browsing = null;

		public function execute() {
			$this->view->add_css("includes/standard.css");

			if (($standard = $this->model->get_standard($_SESSION["standard"])) != false) {
				$this->view->add_tag("standard", $standard["name"]);
			}

			parent::execute();
		}
	}
?>
