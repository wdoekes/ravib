<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_measures_controller extends Banshee\controller {
		private function show_overview() {
			if (($measure_count = $this->model->count_measures($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($categories = $this->model->get_categories($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$paging = new \Banshee\pagination($this->view, "measures", $this->settings->admin_page_size, $measure_count);

			if (($measures = $this->model->get_measures($_SESSION["standard"], $paging->offset, $paging->size)) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$reduce = config_array(MEASURE_REDUCE);

			$this->view->open_tag("overview");

			$category_id = 0;
			$this->view->open_tag("measures");
			foreach ($measures as $measure) {
				list($major) = explode(".", $measure["number"]);
				if ($major != $category_id) {
					$category_id = $major;
					$measure["category"] = $categories[$category_id]["name"];
				}
				$measure["reduce"] = $reduce[$measure["reduce"]];
				$this->view->record($measure, "measure");
			}
			$this->view->close_tag();

			$paging->show_browse_links();

			$this->view->close_tag();
		}

		private function show_measure_form($measure) {
			if (($threats = $this->model->get_threats()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (is_array($measure["threat_links"] ?? null) == false) {
				$measure["threat_links"] = array();
			}

			$this->view->open_tag("edit");

			$this->view->record($measure, "measure");

			$this->view->open_tag("reduces");
			foreach (config_array(MEASURE_REDUCE) as $reduce) {
				$this->view->add_tag("reduce", $reduce);
			}
			$this->view->close_tag();

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$params = array(
					"id"      => $threat["id"],
					"checked" => show_boolean(in_array($threat["id"], $measure["threat_links"])));
				$this->view->open_tag("threat", $params);
				$this->view->add_tag("number", $threat["number"]);
				$this->view->add_tag("title", $threat["number"].". ".$threat["threat"]);
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_css("includes/standard.css");

			if (($standard = $this->model->get_standard($_SESSION["standard"])) != false) {
				$this->view->add_tag("standard", $standard["name"]);
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save measure") {
					/* Save measure
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_measure_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create measure
						 */
						if ($this->model->create_measure($_POST, $_SESSION["standard"]) === false) {
							$this->view->add_message("Error creating measure.");
							$this->show_measure_form($_POST);
						} else {
							$this->user->log_action("measure created");
							$this->show_overview();
						}
					} else {
						/* Update measure
						 */
						if ($this->model->update_measure($_POST, $_SESSION["standard"]) === false) {
							$this->view->add_message("Error updating measure.");
							$this->show_measure_form($_POST);
						} else {
							$this->user->log_action("measure updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete measure") {
					/* Delete measure
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_measure_form($_POST);
					} else if ($this->model->delete_measure($_POST["id"]) === false) {
						$this->view->add_message("Error deleting measure.");
						$this->show_measure_form($_POST);
					} else {
						$this->user->log_action("measure deleted");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New measure
				 */
				$measure = array();
				$this->show_measure_form($measure);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit measure
				 */
				if (($measure = $this->model->get_measure($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Case not found.\n");
				} else {
					$this->show_measure_form($measure);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
