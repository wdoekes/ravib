<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<label for="fullname">Naam:</label>
<input type="text" id="fullname" name="fullname" value="{fullname}" class="form-control" />
<label for="email">E-mailadres:</label>
<input type="text" id="email" name="email" value="{email}" class="form-control" />
<label>Organisatie:</label>
<input type="text" disabled="disabled" value="{organisation}" class="form-control" />
<label for="current">Huidige wachtwoord:</label>
<input type="password" id="current" name="current" class="form-control" />
<label for="password">Nieuw wachtwoord:</label> <span class="blank" style="font-size:10px">(wordt niet aangepast indien leeg)</span>
<input type="password" id="password" name="password" class="form-control" />
<label for="repeat">Herhaal wachtwoord:</label>
<input type="password" id="repeat" name="repeat" class="form-control" />
<xsl:if test="@authenticator='yes'">
<label for="secret">Authenticator code:</label>
<div class="input-group">
	<input type="text" id="secret" name="authenticator_secret" value="{authenticator_secret}" class="form-control" style="text-transform:uppercase" />
	<span class="input-group-btn"><input type="button" value="Genereer" class="btn btn-default" onClick="javascript:set_authenticator_code()" /></span>
</div>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="Account bijwerken" class="btn btn-default" />
</div>
<div class="btn-group">
<a href="/uitloggen" class="btn btn-default">Uitloggen</a>
</div>
<xsl:if test="/output/user/@admin='no'">
<div class="btn-group">
<input type="submit" name="submit_button" value="Account verwijderen" class="btn btn-danger" onClick="javascript:return prompt('Dit account wordt verwijderd. Type \'VERWIJDEREN\' als u het zeker weet.') == 'VERWIJDEREN';" />
</div>
</xsl:if>
</form>

<h2>Recentelijk account activiteit</h2>
<table class="table table-striped table-xs">
<thead>
<tr>
<th>IP-adres</th>
<th>Datum / tijd</th>
<th>Activiteit</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="actionlog/log">
<tr>
<td><xsl:value-of select="ip" /></td>
<td><xsl:value-of select="timestamp" /></td>
<td><xsl:value-of select="message" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div id="help">
<p>De 'Authenticator code' vereist het gebruik van een authenticator app (RFC 6238) op de mobiele telefoon. Deze app moet BASE32 characters, SHA1 en een 30 seconden-tijdinterval gebruiken om een 6 cijferige code te genereren.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Gebruikersaccount</h1>
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
