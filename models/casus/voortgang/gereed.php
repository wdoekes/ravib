<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_voortgang_gereed_model extends ravib_model {
		public function get_data($code) {
			if ($code == null) {
				return false;
			}

			$padding = strlen($code) % 4;
			if ($padding > 0) {
				$padding = 4 - $padding;
				$code .= str_repeat("=", $padding);
			}

			if (($data = json_decode(base64_decode(strtr($code, "-_", "+/")), true)) == null) {
				return false;
			}

			$signature = $data["signature"];
			unset($data["signature"]);

			if ($this->borrow("casus/voortgang")->get_signature($data) != $signature) {
				return false;
			}

			return $data;
		}

		public function get_task($measure_id, $case_id) {
			$query = "select c.name, p.info, p.done, concat(m.number, %s, m.name) as measure, u.fullname ".
			         "from cases c, case_progress p, measures m, users u ".
			         "where c.id=%d and c.id=p.case_id and p.measure_id=%d and m.id=p.measure_id and u.id=p.executor_id";
			if (($result = $this->db->execute($query, " ", $case_id, $measure_id)) == false) {
				return false;
			}
			$task = $result[0];

			$this->decrypt($task, "name", "info");

			return $task;
		}

		public function mark_as_done($measure_id, $case_id) {
			$query = "update case_progress set done=%d where case_id=%d and measure_id=%d";

			return $this->db->query($query, YES, $case_id, $measure_id) !== false;
		}

		public function send_notification($measure_id, $case_id) {
			if (($case = $this->get_case($case_id)) == false) {
				return false;
			} else if (($progress = $this->borrow("casus/voortgang")->get_progress($measure_id, $case_id)) == false) {
				return false;
			} else if (($executor = $this->borrow("casus/voortgang")->get_person($progress["executor_id"])) == false) {
				return false;
			} else if (($measure = $this->db->entry("measures", $measure_id)) == false) {
				return false;
			}

			$query = "select u.fullname, u.email from users u, user_role r ".
			         "where u.id=r.user_id and u.organisation_id=%d and (r.role_id=%d or r.role_id=%d)";
			if (($users = $this->db->execute($query, $case["organisation_id"], ORGANISATION_ADMIN_ROLE_ID, ADMIN_ROLE_ID)) == false) {
				return false;
			}

			if (($reviewer = $this->borrow("casus/voortgang")->get_person($progress["reviewer_id"])) != false) {
				$reviewer = $reviewer["fullname"];
			} else {
				$reviewer = "-";
			}

			if (($message = file_get_contents("../extra/taak_gereed.txt")) === false) {
				exit("Can't load message template.\n");
			}

			$replace = array(
				"REVIEWER"    => $reviewer,
				"CASE"        => $case["name"],
				"INFORMATION" => $progress["info"],
				"MEASURE"     => $measure["number"]." ".$measure["name"],
				"EXECUTOR"    => $executor["fullname"]);

			$mail = new ravib_email("Gereedmelding van taak inzake ".$case["name"], $this->settings->webmaster_email, "RAVIB");
			$mail->set_message_fields($replace);
			$mail->message($message);

			foreach ($users as $user) {
				$mail->to($user["email"], $user["fullname"]);
			}

			return $mail->send();
		}
	}
?>
