<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_measures_export_controller extends Banshee\controller {
		public function execute() {
			if (($categories = $this->model->get_categories($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($measures = $this->model->get_measures($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$reduce = config_array(MEASURE_REDUCE);
			$category_id = 0;

			$csv = new \banshee\CSVfile();

			foreach ($measures as $measure) {
				list($cid) = explode(".", $measure["number"]);
				if ($cid != $category_id) {
					$category_id = $cid;
					$csv->add_line($categories[$category_id]["name"]);
				}

				$csv->add_line($measure["number"], $measure["name"], $reduce[$measure["reduce"]]);
			}

			$csv->to_output($this->view);
		}
	}
?>
