<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />
<xsl:include href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs actors">
<thead class="table-xs">
<tr><th>Actor</th><th>Bereidheid / kans</th><th>Kennisniveau</th><th>Middelen</th><th>Dreiging</th></tr>
</thead>
<tbody>
<xsl:for-each select="actors/actor">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><span class="table-xs">Actor</span><xsl:value-of select="name" /></td>
<td><span class="table-xs">Bereidheid / kans</span><xsl:value-of select="chance" /></td>
<td><span class="table-xs">Kennisniveau</span><xsl:value-of select="knowledge" /></td>
<td><span class="table-xs">Middelen</span><xsl:value-of select="resources" /></td>
<td><span class="table-xs">Dreiging</span><xsl:value-of select="threat" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group">
<a href="/{/output/page}/nieuw" class="btn btn-default">Nieuwe actor</a>
<a href="/dashboard" class="btn btn-default">Terug</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="actor/@id">
<input type="hidden" name="id" value="{actor/@id}" />
</xsl:if>

<label for="name">Naam actor:</label>
<input type="text" id="name" name="name" value="{actor/name}" class="form-control" />
<label for="chance">Bereidheid tot aanval of kans op veroorzaken incident:</label>
<select name="chance" class="form-control">
<xsl:for-each select="chance/item"><option value="{position()}"><xsl:if test="position()=../../actor/chance"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="knowledge">Kennisniveau:</label>
<select name="knowledge" class="form-control">
<xsl:for-each select="knowledge/item"><option value="{position()}"><xsl:if test="position()=../../actor/knowledge"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="resources">Middelen:</label>
<select name="resources" class="form-control">
<xsl:for-each select="resources/item"><option value="{position()}"><xsl:if test="position()=../../actor/resources"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="reason">Doel van aanval of oorzaak van incident:</label>
<input type="text" id="reason" name="reason" value="{actor/reason}" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Actor opslaan" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Afbreken</a>
<xsl:if test="actor/@id">
<input type="submit" name="submit_button" value="Actor verwijderen" class="btn btn-default" onClick="javascript:return confirm('VERWIJDEREN: Weet u het zeker?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/actoren.png" class="title_icon" />
<xsl:apply-templates select="breadcrumbs" />
<h1>Actoren</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
<div id="help">
<p>Op deze pagina geeft u de actoren aan die een concreet beveiligingsincident kunnen veroorzaken. Een incident kan het gevolg zijn van een moedwillige actie, waarbij u moet kijken naar de bereidheid van een actor om u daadwerkelijk aan te vallen. Het kennisniveau geeft de kennis van de actor op het gebied van cybersecurity en digitale aanvallen aan.</p>
<p>Een incident kan ook optreden als gevolg van een onbedoelde actie. U kijkt daarbij niet naar de bereidheid van de actor, maar de kans dat een actor een incident veroorzaakt. Denk bij de oorzaak aan (deadline)stress, onvoldoende sturing of beleid of een gebrek aan interesse of bewustzijn. Omdat een actor in zo'n geval geen middelen inzet, kies bij middelen 'n.v.t.'.</p>
<p>Het is toegestaan om een actor meerdere malen te noemen, waarbij verschillende waarden voor kans/bereidheid en kennisniveau worden gekozen. Bijvoorbeeld:</p>
<table class="table table-condensed table-striped">
<thead>
<tr><th>Actor</th><th>Bereidheid / kans</th><th>Kennisniveau</th><th>Middelen</th></tr>
</thead>
<tbody>
<tr><td>Medewerkers</td><td>gering</td><td>basis</td><td>n.v.t.</td></tr>
<tr><td>Medewerkers (ICT-er)</td><td>gering</td><td>kundig</td><td>n.v.t.</td></tr>
<tr><td>Medewerkers (ontevreden)</td><td>mogelijk</td><td>basis</td><td>beperkt</td></tr>
</tbody>
</table>
<p>In de eerste stap van de risicoanalyse bepaalt u de scope van de risicoanalyse. Deze stap biedt u de mogelijkheid om een handout te maken voor de aanwezigen bij de risicoanalyse waar deze scope op te zien is. Op die handout worden tevens deze actoren vermeld.</p>
</div>
</xsl:template>

</xsl:stylesheet>
