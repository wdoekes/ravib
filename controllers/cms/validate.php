<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_validate_controller extends Banshee\controller {
		public function execute() {
			$this->view->add_css("includes/standard.css");

			if (($standard = $this->model->get_standard($_SESSION["standard"])) != false) {
				$this->view->add_tag("standard", $standard["name"]);
			}

			if (($threats = $this->model->linked_threats($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($measures = $this->model->linked_measures($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("validate");

			$this->view->open_tag("threats");
			foreach ($threats as $threat) {
				$this->view->record($threat, "threat");
			}
			$this->view->close_tag();

			$this->view->open_tag("measures");
			foreach ($measures as $measure) {
				$this->view->record($measure, "measure");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}
	}
?>
