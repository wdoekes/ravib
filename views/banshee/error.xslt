<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="main.xslt" />

<xsl:template match="content">
<h1>Error</h1>
<img src="/images/error.png" alt="error" class="error" />
<p><xsl:apply-templates select="website_error" /></p>
<p>Indien u van mening bent dat het hier om een fout in de website gaat, stuur dan een bericht naar de <a href="mailto:{webmaster_email}">beheerder van deze website</a>. Klik <a href="/">hier</a> om terug te gaan naar de homepage.</p>
</xsl:template>

</xsl:stylesheet>
