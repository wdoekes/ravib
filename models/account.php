<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class account_model extends Banshee\model {
		private $hashed = null;

		public function get_organisation() {
			if (($result = $this->db->entry("organisations", $this->user->organisation_id)) == false) {
				return false;
			}

			return $result["name"];
		}

		public function last_account_logs() {
			if (($fp = fopen("../logfiles/actions.log", "r")) == false) {
				return false;
			}

			$result = array();

			while (($line = fgets($fp)) !== false) {
				$parts = explode("|", chop($line));
				if (count($parts) < 4) {
					continue;
				}

				list($ip, $timestamp, $user_id, $message) = $parts;

				if ($user_id == "-") {
					continue;
				} else if ($user_id != $this->user->id) {
					continue;
				}

				array_push($result, array(
					"ip"        => $ip,
					"timestamp" => $timestamp,
					"message"   => $message));
				if (count($result) > 15) {
					array_shift($result);
				}
			}

			fclose($fp);

			return array_reverse($result);
		}

		public function profile_oke($profile) {
			$result = true;

			if (trim($profile["fullname"]) == "") {
				$this->view->add_message("Vul uw naam in.");
				$result = false;
			}

			if (valid_email($profile["email"]) == false) {
				$this->view->add_message("Ongeldig e-mailadres.");
				$result = false;
			} else if (($check = $this->db->entry("users", $profile["email"], "email")) != false) {
				if ($check["id"] != $this->user->id) {
					$this->view->add_message("E-mailadres is al in gebruik.");
					$result = false;
				}
			}

			if (strlen($profile["current"]) > PASSWORD_MAX_LENGTH) {
				$this->view->add_message("Het wachtwoord is te lang.");
				$result = false;
			} else if (password_verify($profile["current"], $this->user->password) == false) {
				$this->view->add_message("Het huidige wachtwoord is incorrect.");
				$result = false;
			}

			if ($profile["password"] != "") {
				if (is_secure_password($profile["password"], $this->view) == false) {
					$result = false;
				} else if ($profile["password"] != $profile["repeat"]) {
					$this->view->add_message("De nieuwe wachtwoorden komen niet overeen.");
					$result = false;
				} else if (password_verify($profile["password"], $this->user->password)) {
					$this->view->add_message("Het nieuwe wachtwoord moet anders zijn dan het huidige wachtwoord.");
					$result = false;
				}

			}

			if (is_true(USE_AUTHENTICATOR)) {
				if ((strlen($profile["authenticator_secret"]) > 0) && ($profile["authenticator_secret"] != str_repeat("*", 16))) {
					if (valid_input($profile["authenticator_secret"], Banshee\authenticator::BASE32_CHARS, 16) == false) {
						$this->view->add_message("Ongeldige authenticator code.");
						$result = false;
					}
				}
			}

			return $result;
		}

		public function update_profile($profile) {
			$keys = array("fullname", "email");

			if ($profile["password"] != "") {
				array_push($keys, "password");
				array_push($keys, "status");
				if (ENCRYPT_DATA) {
					array_push($keys, "crypto_key");

					$aes = new \Banshee\Protocol\AES256($profile["current"]);
					$crypto_key = $aes->decrypt($this->user->crypto_key);

					$aes = new \Banshee\Protocol\AES256($profile["password"]);
					$profile["crypto_key"] = $aes->encrypt($crypto_key);
				}

				$profile["password"] = password_hash($profile["password"], PASSWORD_ALGORITHM);
				$profile["status"] = USER_STATUS_ACTIVE;
			}

			if (is_true(USE_AUTHENTICATOR)) {
				if ($profile["authenticator_secret"] != str_repeat("*", 16)) {
					array_push($keys, "authenticator_secret");
					if (trim($profile["authenticator_secret"]) == "") {
						$profile["authenticator_secret"] = null;
					}
				}
			}

			return $this->db->update("users", $this->user->id, $profile, $keys) !== false;
		}

		public function delete_oke() {
			if ($this->user->has_role(ORGANISATION_ADMIN_ROLE_ID)) {
				$query = "select count(*) as count from users u, user_role p ".
				         "where u.organisation_id=%d and u.id!=%d and u.id=p.user_id and p.role_id=%d";
				if (($result = $this->db->execute($query, $this->user->organisation_id, $this->user->id, ORGANISATION_ADMIN_ROLE_ID)) === false) {
					$this->view->add_message("Database error.");
					return false;
				}
				$admin_accounts = $result[0]["count"];

				$query = "select count(*) as count from users where organisation_id=%d and id!=%d";
				if (($result = $this->db->execute($query, $this->user->organisation_id, $this->user->id)) === false) {
					$this->view->add_message("Database error.");
					return false;
				}
				$user_accounts = $result[0]["count"] - $admin_accounts;

				if (($admin_accounts == 0) && ($user_accounts > 0)) {
					$this->view->add_message("Dit account is de enige met beheerrechten voor uw organisatie en kan daarom niet worden verwijderd.");
					return false;
				}
			}

			return true;
		}

		public function delete_account() {
			if ($this->user->is_admin) {
				return false;
			}

			if ($this->borrow("cms/user")->delete_user($this->user->id) === false) {
				return false;
			}

			/* Count users left
			 */
			$query = "select count(*) as count from users where organisation_id=%d";
			if (($result = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			/* No users left?
			 */
			if ($result[0]["count"] == 0) {
				/* Delete organisation
				 */
				if ($this->borrow("cms/organisation")->delete_organisation($this->user->organisation_id) === false) {
					return false;
				}
			}

			return true;
		}
	}
?>
