<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_rapportage_model extends ravib_model {
		private function get_case_threats($case_id) {
			if (($threats = $this->borrow("casus/dreigingen")->get_case_threats($case_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($threats as $threat) {
				$result[$threat["id"]] = $threat;
			}

			return $result;
		}

		private function get_measures($case_id) {
			$query = "select distinct m.* from measures m, case_threat_measure l, case_threats t, cases c ".
					 "where m.id=l.measure_id and l.case_threat_id=t.id and t.case_id=c.id and c.id=%d ".
			         "and m.standard_id=c.standard_id order by id";

			if (($measures = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			$query = "select l.* from case_threat_measure l, case_threats t ".
					 "where l.case_threat_id=t.id and t.case_id=%d and measure_id=%d order by t.id";
			foreach ($measures as $m => $measure) {
				if (($threats = $this->db->execute($query, $case_id, $measure["id"])) === false) {
					return false;
				}

				$measures[$m]["threats"] = array();
				foreach ($threats as $threat) {
					array_push($measures[$m]["threats"], $threat["case_threat_id"]);
				}
			}

			return $measures;
		}

		private function get_systems_for_threat($threat_id, $case_id) {
			$query = "select b.* from bia b, case_threat_bia t, case_scope s ".
					 "where b.id=t.bia_id and t.case_threat_id=%d and s.bia_id=b.id and s.case_id=%d";

			if (($systems = $this->db->execute($query, $threat_id, $case_id)) === false) {
				return false;
			}

			foreach ($systems as $key => $system) {
				$this->decrypt($systems[$key], "item", "description", "impact");

				$a = $system["availability"] - 1;
				$i = $system["integrity"] - 1;
				$c = $system["confidentiality"] - 1;
				$systems[$key]["value"] = $this->asset_value[$i][$c][$a] ?? 0;
			}

			return $systems;
		}

		private function sort_threats($threat_a, $threat_b) {
			if (($threat_a["relevant"] == false) && $threat_b["relevant"]) {
				return 1;
			} else if ($threat_a["relevant"] && ($threat_b["relevant"] == false)) {
				return -1;
			}

			if ($threat_a["risk_value"] < $threat_b["risk_value"]) {
				return 1;
			} else if ($threat_a["risk_value"] > $threat_b["risk_value"]) {
				return -1;
			}

			$systems_a = is_array($threat_a["systems"]) ? count($threat_a["systems"]) : 0;
			$systems_b = is_array($threat_b["systems"]) ? count($threat_b["systems"]) : 0;
			if ($systems_a < $systems_b) {
				return 1;
			} elseif ($systems_a > $systems_b) {
				return -1;
			}

			if (($threat_a["number"] ?? 0) > ($threat_b["number"] ?? 0)) {
				return 1;
			} else if (($threat_a["number"] ?? 0) < ($threat_b["number"] ?? 0)) {
				return -1;
			}

			return 0;
		}

		private function sort_measures($measure_a, $measure_b) {
			if ((($measure_a["risk"] ?? "") == "") && (($measure_b["risk"] ?? "") != "")) {
				return 1;
			} else if ((($measure_a["risk"] ?? "") != "") && (($measure_b["risk"] ?? "") == "")) {
				return -1;
			}

			if (is_false($measure_a["relevant"]) && is_true($measure_b["relevant"])) {
				return 1;
			} else if (is_true($measure_a["relevant"]) && is_false($measure_b["relevant"])) {
				return -1;
			}

			if ($measure_a["urgency"] < $measure_b["urgency"]) {
				return 1;
			} else if ($measure_a["urgency"] > $measure_b["urgency"]) {
				return -1;
			}

			if (($measure_a["asset_value"] ?? 0) < ($measure_b["asset_value"] ?? 0)) {
				return 1;
			} else if (($measure_a["asset_value"] ?? 0) > ($measure_b["asset_value"] ?? 0)) {
				return -1;
			}

			return version_compare($measure_a["number"], $measure_b["number"]);
		}

		private function valid_url($url) {
			$parts = explode("/", $url, 4);
			if (count($parts) < 4) {
				return false;
			}

			list($protocol,, $hostname, $path) = $parts;

			switch ($protocol) {
				case "http:": $http = new \Banshee\Protocol\HTTP($hostname); break;
				case "https:": $http = new \Banshee\Protocol\HTTPS($hostname); break;
				default: return false;
			}

			if (($result = $http->GET("/".$path)) == false) {
				return false;
			}

			if ($result["status"] != 200) {
				return false;
			}

			return true;
		}

		public function draw_risk_matrix($pdf, $threats, $relevant) {
			$count = 0;

			$matrix = array();
			foreach ($threats as $threat) {
				if (($threat["chance"] == 0) || ($threat["impact"] == 0)) {
					continue;
				} else if ($relevant == ($threat["handle"] == THREAT_ACCEPT)) {
					continue;
				}

				if (is_array($matrix[$threat["chance"] - 1] ?? null) == false) {
					$matrix[$threat["chance"] - 1] = array_fill(0, count($this->risk_matrix_impact), "");
				}

				$matrix[$threat["chance"] - 1][$threat["impact"] - 1]++;
				$count++;
			}

			$pdf->SetFont("helvetica", "", 10);

			$cell_width = 29;
			$cell_height = 8;

			$pdf->Cell($cell_width, $cell_height);
			$pdf->Cell(150, $cell_height, "impact", 0, 0, "C");
			$pdf->Ln();
			$pdf->Cell($cell_width, $cell_height, "kans");
			foreach ($this->risk_matrix_impact as $impact) {
				$pdf->Cell($cell_width, $cell_height, $impact, 1, 0, "C");
			}
			$pdf->Ln();
			$max_y = count($this->risk_matrix_impact) - 1;
			foreach (array_reverse($this->risk_matrix_chance) as $y => $chance) {
				$pdf->Cell($cell_width, $cell_height, $chance, 1);
				foreach ($this->risk_matrix_impact as $x => $impact) {
					$risk = $this->risk_matrix_labels[$this->risk_matrix[$max_y - $y][$x]];
					$pdf->SetColor($risk);
					$pdf->Cell($cell_width, $cell_height, $matrix[$max_y - $y][$x] ?? "", 1, 0, "C", true);
				}
				$pdf->Ln();
			}

			$pdf->Ln(8);

			return $count;
		}

		public function generate_report($case) {
			/* Get threats
			 */
			if (($threats = $this->get_case_threats($case["id"])) === false) {
				return;
			}

			foreach ($threats as $t => $threat) {
				$threats[$t]["risk_value"] = $this->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
				$threats[$t]["relevant"] = ($threat["handle"] != THREAT_ACCEPT) && ($threat["handle"] > 0);
				$threats[$t]["systems"] = $this->get_systems_for_threat($threat["id"], $case["id"]);
			}

			if (is_true($_POST["sort_by_risk"] ?? false)) {
				uasort($threats, array($this, "sort_threats"));
			}

			$nr = 1;
			foreach ($threats as $t => $threat) {
				if ($threats[$t]["relevant"] || is_true($_POST["accepted_risks"] ?? false)) {
					$threats[$t]["number"] = $nr++;
				}
			}

			/* Get measures
			 */
			if (($measures = $this->get_measures($case["id"])) === false) {
				return;
			}

			foreach ($measures as $m => $measure) {
				$measures[$m]["urgency"] = 0;
				$measures[$m]["relevant"] = false;

				foreach ($measure["threats"] as $threat_id) {
					$threat = $threats[$threat_id];

					if ($threat["risk_value"] > $measures[$m]["urgency"]) {
						$measures[$m]["urgency"] = $threat["risk_value"];
					}

					if ($threat["relevant"]) {
						$measures[$m]["relevant"] = true;
					}
				}
			}

			if (is_true($_POST["sort_by_risk"] ?? false)) {
				usort($measures, array($this, "sort_measures"));
			}

			/* Get BIA items
			 */
			if (($bia_items = $this->borrow("casus/scope")->get_scope($case["id"])) === false) {
				return false;
			}

			foreach ($bia_items as $i => $item) {
				$query = "select case_threat_id from case_threat_bia b, case_threats t ".
						 "where t.id=b.case_threat_id and b.bia_id=%d and t.case_id=%d";
				if (($ts = $this->db->execute($query, $item["id"], $case["id"])) === false) {
					return false;
				}

				$bia_items[$i]["threats"] = array();
				foreach ($ts as $t) {
					if (($threat = $threats[$t["case_threat_id"]]) == null) {
						return false;
					}
					if (isset($bia_items[$i]["threats"][$threat["risk_value"]]) == false) {
						$bia_items[$i]["threats"][$threat["risk_value"]] = 1;
					} else {
						$bia_items[$i]["threats"][$threat["risk_value"]]++;
					}
				}
			}

			/* Get scenarios
			 */
			if (($scenarios = $this->borrow("casus/scenarios")->get_scenarios($case["id"])) === false) {
				return false;
			}

			$ra_date = date_string("j F Y", strtotime($case["date"]));
			$ra_standard = $this->get_standard($case["standard_id"]);

			/* Generate report
			 */
			$pdf = new RAVIB_report($case["title"]);
			$pdf->SetAuthor($this->get_organisation($this->user->organisation_id)." en RAVIB");
			$pdf->SetSubject("Rapportage risicoanalyse voor informatiebeveiliging");
			$pdf->SetKeywords("RAVIB, risicoanalyse, informatiebeveiliging, rapportage");
			$pdf->AliasNbPages();

			/* Title
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Titelpagina");
			if ($case["logo"] != "") {
				if ($this->valid_url($case["logo"])) {
					try {
						$pdf->Image($case["logo"], 15, 20, 0, 25);
					} catch (Exception $e) {
						$pdf->SetFont("helvetica", "", 8);
						$pdf->Cell(0, 0, "[ can't load logo ]");
					}
				} else {
					$pdf->SetFont("helvetica", "", 8);
					$pdf->Cell(0, 0, "[ invalid logo url ]");
				}
			}

			$pdf->SetFont("helvetica", "B", 16);
			$pdf->SetTextColor(54, 94, 145);
			$pdf->Ln(95);
			$pdf->Cell(0, 0, "Rapportage risicoanalyse", 0, 1, "C");
			$pdf->Ln(7);
			$pdf->Cell(0, 0, "voor informatiebeveiliging", 0, 1, "C");
			$pdf->SetFont("helvetica", "", 12);
			$pdf->SetTextColor(64, 64, 64);
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $case["title"], 0, 1, "C");
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $ra_date, 0, 1, "C");
			$pdf->Ln(40);
			$pdf->SetFont("helvetica", "I", 12);
			$pdf->Cell(0, 0, "Vertrouwelijk", 0, 1, "C");

			/* Titel achterblad
			 */
			$pdf->AddPage();
			$pdf->Image("images/layout/ravib_logo.png", 15, 240, 40, 0);
			$pdf->SetFont("helvetica", "", 8);
			$pdf->SetTextColor(128, 128, 128);
			$pdf->Ln(228);
			$pdf->Write(5, "De risicoanalyse waar deze rapportage over gaat, is uitgevoerd met behulp van RAVIB. RAVIB is een gratis en open source methodiek voor het uitvoeren van een risicoanalyse voor informatiebeveiliging. Deze methodiek is de vinden op https://www.ravib.nl/.");

			$pdf->SetFont("helvetica", "", 12);
			$pdf->SetTextColor(0, 0, 0);

			/* Text
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Uitleg");

			$template = file("../extra/report.txt");
			if (is_true($_POST["extra_info"] ?? false)) {
				$template = array_merge($template, file("../extra/report_extra.txt"));
			}

			$text = array();
			$i = 0;
			foreach ($template as $line) {
				switch (substr($line, 0, 1)) {
					case "#":
						break 2;
					case "*":
						$text[++$i] = trim($line);
						break;
					case ">":
					case "-":
						$text[$i++] = trim($line);
						break;
					case "\n":
						$i++;
						break;
					default:
						$line = str_replace("[DATE]", $ra_date, $line);
						$line = str_replace("[ORGANISATION]", $case["organisation"], $line);
						$line = str_replace("[STANDARD]", $ra_standard, $line);
						$text[$i] = trim(($text[$i] ?? "")." ".trim($line));

				}
			}

			$new_page = true;
			$list = false;
			foreach ($text as $i => $line) {
				switch (substr($line, 0, 1)) {
					case ">":
						if ($new_page == false) {
							$pdf->Ln(4);
						}
						if ($list) {
							$pdf->Ln(3);
						}
						$pdf->AddChapter(substr($line, 2));
						$new_page = false;
						$list = false;
						break;
					case "-":
						$pdf->AddPage();
						$new_page = true;
						$list = false;
						break;
					case "*":
						list($head, $line) = explode(":", $line, 2);
						$pdf->Write(5, "» ");
						$pdf->SetFont("helvetica", "I", 10);
						$pdf->Write(5, trim(substr($head, 1)).": ");
						$pdf->SetFont("helvetica", "", 10);
						$pdf->Write(5, $line);
						$pdf->Ln(5);
						$list = true;
						break;
					default:
						if ($list) {
							$pdf->Ln(3);
						}
						$pdf->Write(5, $line);
						$pdf->Ln(8);
						$list = false;
				}
			}

			/* Scenarios
			 */
			if (count($scenarios) > 0) {
				$pdf->AddPage();
				$pdf->Bookmark("Scenario's");
				$pdf->AddChapter("Scenario's");

				$pdf->Write(5, "Dit hoofdstuk bevat mogelijke scenario's, op basis van de concrete dreigingen uit een volgend hoofdstuk.");
				$pdf->Ln(8);

				foreach ($scenarios as $scenario) {
					$pdf->SetFont("helvetica", "B", 11);
					$pdf->Write(5, $scenario["title"]);
					$pdf->Ln(5);
					$pdf->SetFont("helvetica", "", 10);
					$pdf->Write(5, $scenario["scenario"]);

					if ($scenario["consequences"] != "") {
						$pdf->Ln(8);
						$pdf->SetFont("helvetica", "I", 10);
						$pdf->Write(5, "Mogelijke gevolgen");
						$pdf->Ln(5);
						$pdf->SetFont("helvetica", "", 10);
						$pdf->Write(5, $scenario["consequences"]);
					}

					$pdf->Ln(10);
				}
			}

			/* Risk matrix
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Risicomatrices");
			$pdf->AddChapter("Risicomatrix van de dreigingen");
			$pdf->Write(5, "In onderstaande tabel zijn de dreigingen waarvan het restrisico wordt geaccepteerd, niet meegenomen.");
			$pdf->Ln(8);
			$this->draw_risk_matrix($pdf, $threats, true);

			/* Measure matrix
			 */
			$pdf->AddChapter("Verdeling urgentie maatregelen uit ".$ra_standard);
			$pdf->Write(5, "In onderstaande tabel zijn de maatregelen tegen de dreigingen waarvan het restrisico wordt geaccepteerd, niet meegenomen.");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			$matrix = array();
			foreach ($measures as $measure) {
				$highest_risk = -1;

				foreach ($threats as $threat) {
					if (in_array($threat["id"], $measure["threats"]) == false) {
						continue;
					}
					if (($threat["chance"] == 0) || ($threat["impact"] == 0) || ($threat["handle"] == 0)) {
						continue;
					}
					if ($threat["relevant"] == false) {
						continue;
					}
					$risk = $this->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
					if ($risk >= $highest_risk) {
						$highest_risk = $risk;
					}
				}

				$matrix[$highest_risk] = ($matrix[$highest_risk] ?? 0) + 1;
			}

			foreach ($this->risk_matrix_labels as $i => $level) {
				$pdf->Cell(30, 8, $level, 1, 0, "C");
			}
			$pdf->Ln(8);
			foreach ($this->risk_matrix_labels as $i => $level) {
				$pdf->SetColor($this->risk_matrix_labels[$i]);
				$pdf->Cell(30, 8, sprintf("%d", $matrix[$i] ?? ""), 1, 0, "C", true);
			}
			$pdf->Ln(15);

			/* Accepted risk matrix
			 */
			$pdf->AddChapter("Risicomatrix van de geaccepteerde dreigingen");
			$pdf->Write(5, "Onderstaande tabel bevat een overzicht van de dreigingen waarvan het restrisico wordt geaccepteerd.");
			$pdf->Ln(8);
			$accepted_risks = $this->draw_risk_matrix($pdf, $threats, false);

			/* Impact values
			 */
			$impact_values = json_decode($case["impact"], true);
			if ($impact_values[0] != "") {
				$pdf->AddChapter("Invulling van de impact");
				$pdf->Write(5, "De impact heeft voor deze risicoanalyse de volgende invulling:");
				$pdf->Ln(8);

				foreach ($this->risk_matrix_impact as $i => $impact) {
					$pdf->SetFont("helvetica", "B", 10);
					$pdf->Cell(27, 5, $impact.": ");
					$pdf->SetFont("helvetica", "", 10);
					$pdf->Write(5, $impact_values[$i]);
					$pdf->Ln(5);
				}
			}

			/* Information systems
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Scope");
			$pdf->AddChapter("Scope");
			$scope = rtrim($case["scope"], ".").".";
			$pdf->Write(5, "De uitgevoerde risicoanalyse heeft zich beperkt tot ".$scope);
			$pdf->Ln(10);

			$this->borrow("casus/belangen")->add_bia($pdf, $bia_items);

			if ($case["interests"] != "") {
				$pdf->Ln(10);
				$pdf->AddChapter("Te beschermen belangen");
				$pdf->Write(5, $case["interests"]);
			}

			if ($pdf->GetY() > 50) {
				$pdf->AddPage();
			} else {
				$pdf->Ln(10);
			}
			$pdf->Write(5, "Onderstaand overzicht geeft per informatiesysteem het aantal dreigingen en de hoogte ervan aan.");
			$pdf->Ln(10);

			$pdf->Cell(75, 6, "", 0, 0);
			foreach ($this->risk_matrix_labels as $label) {
				$pdf->Cell(26, 6, $label, 0, 0, "C");
			}
			$pdf->Ln(6);

			foreach ($bia_items as $item) {
				if ($item["scope"] == NO) {
					continue;
				}

				$pdf->Cell(75, 6, $item["item"], 0, 0);
				foreach ($this->risk_matrix_labels as $i => $label) {
					$pdf->SetColor($label);
					$risk = $item["threats"][$i] ?? "";
					$pdf->Cell(26, 6, $risk, 1, 0, "C", true);
				}
				$pdf->Ln(6);
			}

			$pdf->Ln(15);

			/* Actors
			 */
			$pdf->AddChapter("Actoren");
			$pdf->Write(5, "De actoren die een inbreuk kunnen maken op de beveiliging.");
			$pdf->Ln(7);

			$this->borrow("casus/belangen")->add_actors($pdf, $case["id"]);

			/* Threats
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Dreigingen");
			$pdf->AddChapter("Dreigingen");
			$pdf->Write(5, "Dit hoofdstuk bevat een overzicht van alle dreigingen en informatie die is aangereikt tijdens de dreigingsanalyse.");
			if (is_false($_POST["accepted_risks"] ?? false) && ($accepted_risks > 0)) {
				$pdf->Write(5, "Dreigingen waarvan het restrisico wordt geaccepteerd zijn in dit hoofdstuk weggelaten.");
			}
			$pdf->Ln(12);

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(7, 6, "", "B");
			$pdf->Cell(153, 6, "Dreiging", "B");
			$pdf->Cell(0, 6, "Risico", "B");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			foreach ($threats as $threat) {
				$risk = $this->risk_matrix_labels[$threat["risk_value"]];

				if ($threat["relevant"] == false) {
					if (is_false($_POST["accepted_risks"] ?? false)) {
						continue;
					} else {
						$risk = "(".$risk.")";
					}
				}

				$pdf->Cell(7, 5, $threat["number"].":");
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->MultiCell(165, 5, $threat["threat"]);
				$pdf->SetFont("helvetica", "", 10);

				$chance = $this->risk_matrix_chance[$threat["chance"] - 1];
				$impact = $this->risk_matrix_impact[$threat["impact"] - 1];

				if ($threat["actor_id"] != null) {
					$actor = $this->borrow("actoren")->get_actor($threat["actor_id"]);

					$pdf->Cell(7, 5);
					$pdf->Write(5, "Meest bedreigende actor: ".$actor["name"]);
					$pdf->Ln(5);
				}

				$pdf->Cell(7, 5);
				$pdf->Cell(40, 5, "Kans: ".$chance);
				$pdf->Cell(40, 5, "Impact: ".$impact);
				$pdf->Cell(68, 5, "Aanpak: ".$this->threat_handle_labels[$threat["handle"] - 1]);

				if ($threat["relevant"]) {
					$pdf->SetColor($risk);
				} else {
					$pdf->SetColor();
				}
				$pdf->Cell(0, 5, $risk, 0, 0, "C", true);
				$pdf->Ln(5);

				if ($threat["action"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, "Gewenste situatie / te nemen actie:");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $threat["action"]);
				}

				if ($threat["current"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, "Huidige situatie / huidige maatregelen:");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $threat["current"]);
				}

				if ($threat["argumentation"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, "Argumentatie voor gemaakte keuze:");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $threat["argumentation"]);
				}

				if ($threat["systems"] != false) {
					$list = array();
					foreach ($threat["systems"] as $system) {
						array_push($list, $system["item"]);
					}

					$pdf->Cell(7, 5);
					$pdf->MultiCell(163, 5, "Betrokken systemen: ".implode(", ", $list));
				}

				$pdf->Ln(1);

				$nr++;
			}

			/* Measures
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Maatregelen");

			$reduce = config_array(MEASURE_REDUCE);
			$reduce_after = array("op", "van", "op/van");

			$pdf->AddChapter("Maatregelen uit ".$ra_standard);
			$pdf->Write(5, "Dit hoofdstuk bevat een overzicht van de maatregelen uit de ".$ra_standard." standaard die zijn geselecteerd op basis van de opgegeven dreigingen. Gebruik de bijbehorende teksten uit deze standaard om tot een concreet plan van aanpak te komen.");
			$pdf->Ln(12);

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(15, 6, "", "B");
			$pdf->Cell(143, 6, "Maatregel", "B");
			$pdf->Cell(0, 6, "Urgentie", "B");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			foreach ($measures as $measure) {
				if (is_false($measure["relevant"] ?? false) && is_false($_POST["accepted_risks"] ?? false)) {
					continue;
				}

				$pdf->Cell(15, 5, $measure["number"]);
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(140, 5, $measure["name"]);
				$pdf->SetFont("helvetica", "", 10);

				$urgency = $this->risk_matrix_labels[$measure["urgency"]];
				if ($measure["relevant"]) {
					$pdf->SetColor($urgency);
				} else {
					$pdf->SetColor();
					$urgency = "(".$urgency.")";
				}
				$pdf->Cell(0, 5, $urgency, 0, 0, "C", true);
				$pdf->Ln(5);

				$pdf->Cell(15, 5, "");
				$pdf->Cell(100, 5, "Deze maatregel vermindert de ".$reduce[$measure["reduce"]]." ".$reduce_after[$measure["reduce"]]." een incident.");
				$pdf->Ln(5);

				$pdf->SetColor();
				foreach ($measure["threats"] as $threat_id) {
					$threat = $threats[$threat_id];

					if (is_false($_POST["accepted_risks"] ?? false)) {
						if ($threat["relevant"] == false) {
							continue;
						}
						if (($threat["chance"] == 0) || ($threat["impact"] == 0) || ($threat["handle"] == 0)) {
							continue;
						}
					}

					$pdf->Cell(15, 5, "");
					$risk = $this->risk_matrix_labels[$threat["risk_value"]];
					$handle = $this->threat_handle_labels[$threat["handle"] - 1];

					$line = sprintf("%s: %s [ %s, %s ]", $threat["number"], $threat["threat"], $risk, $handle);
					$pdf->MultiCell(150, 5, $line);
				}
				$pdf->Ln(2);
			}

			return $pdf;
		}
	}
?>
