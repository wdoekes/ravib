<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class account_controller extends Banshee\controller {
		private function show_profile_form($profile = null) {
			if ($profile === null) {
				$profile = array(
					"fullname"             => $this->user->fullname,
					"email"                => $this->user->email,
					"authenticator_secret" => str_repeat("*", strlen($this->user->authenticator_secret ?? "")));
			}

			if (($organisation = $this->model->get_organisation()) === false) {
				$this->view->add_tag("result", "Database error.");
				return false;
			}

			if (is_true(USE_AUTHENTICATOR)) {
				$this->view->add_javascript("webui/jquery-ui.js");
				$this->view->add_javascript("account.js");

				$this->view->add_css("webui/jquery-ui.css");
			}

			$this->view->add_help_button();

			$this->view->open_tag("edit", array("authenticator" => show_boolean(USE_AUTHENTICATOR)));

			$this->view->add_tag("username", $this->user->username);
			$this->view->add_tag("fullname", $profile["fullname"]);
			$this->view->add_tag("organisation", $organisation);
			$this->view->add_tag("email", $profile["email"]);
			$this->view->add_tag("authenticator_secret", $profile["authenticator_secret"]);

			/* Action log
			 */
			if (($actionlog = $this->model->last_account_logs()) !== false) {
				$this->view->open_tag("actionlog");
				foreach ($actionlog as $log) {
					$this->view->record($log, "log");
				}
				$this->view->close_tag();
			}

			$this->view->close_tag();
		}

		public function execute() {
			if ($this->user->logged_in == false) {
				$this->view->add_tag("result", "You are not logged in!", array("url" => $this->settings->start_page));
				return;
			}

			$this->view->description = "Account";
			$this->view->keywords = "account";
			$this->view->title = "Account";

			if ($this->user->status == USER_STATUS_CHANGEPWD) {
				$this->view->add_message("Wijzig a.u.b. uw wachtwoord.");
			}

			if (isset($_SESSION["profile_next"]) == false) {
				if ($this->page->pathinfo[0] == "profile") {
					$_SESSION["profile_next"] = $this->settings->start_page;
				} else {
					$_SESSION["profile_next"] = substr($_SERVER["REQUEST_URI"], 1);
				}
			}

			if ($this->page->parameter_value(0, "authenticator") && $this->page->ajax_request) {
				$authenticator = new \Banshee\authenticator;
				$this->view->add_tag("secret", $authenticator->create_secret());
			} else if ($_SERVER["REQUEST_METHOD"] == "POST") {
				/* Update profile
				 */
				if ($_POST["submit_button"] == "Account bijwerken") {
					if ($this->model->profile_oke($_POST) == false) {
						$this->show_profile_form($_POST);
					} else if ($this->model->update_profile($_POST) === false) {
						$this->view->add_tag("result", "Fout tijdens bijwerken van uw account.", array("url" => "profile"));
					} else {
						$this->view->add_tag("result", "Uw account is bijgewerkt.", array("url" => $_SESSION["profile_next"]));
						$this->user->log_action("profile updated");
						unset($_SESSION["profile_next"]);
					}
				} else if ($_POST["submit_button"] == "Account verwijderen") {
					if ($this->model->delete_oke() == false ) {
						$this->show_profile_form();
					} else if ($this->model->delete_account() == false) {
						$this->view->add_message("Fout tijdens het verwijderen van het account.");
						$this->show_profile_form();
					} else {
						$this->view->add_tag("result", "Uw account is verwijderd. U bent uitgelogd.", array("url" => ""));
						$this->user->logout();
					}
				}
			} else {
				$this->show_profile_form();
			}
		}
	}
?>
