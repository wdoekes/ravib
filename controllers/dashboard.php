<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class dashboard_controller extends ravib_controller {
		protected $prevent_repost = false;

		public function execute() {
			if (isset($_SESSION["advisor_organisation_id"]) != false) {
				if (($organisation = $this->model->get_organisation($_SESSION["advisor_organisation_id"])) != false) {
					$this->view->add_system_warning("U treedt nu op als adviseur voor %s.", $organisation);
				}
			}

			if (($threats = $this->model->get_threats()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($measures = $this->model->get_measures()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($systems = $this->model->get_bia()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($progress = $this->model->get_progress()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("dashboard");

			$menu = array(
				"bia"      => "BIA",
				"actoren"  => "Actoren",
				"casus"    => "Casussen",
				"data"     => "Databeheer",
				"adviseur" => "Adviseur");

			$this->view->open_tag("menu");
			foreach ($menu as $link => $label) {
				if ($this->user->access_allowed($link)) {
					$this->view->add_tag("item", $label, array("link" => $link));
				}
			}
			$this->view->close_tag();

			$this->view->open_tag("threats");
			foreach ($threats as $i => $threat) {
				$this->view->add_tag("threat", $this->model->risk_matrix_labels[$i], array("value" => $threat));
			}
			$this->view->close_tag();

			$this->view->open_tag("measures");
			foreach ($measures as $i => $measure) {
				$this->view->add_tag("measure", $this->model->risk_matrix_labels[$i], array("value" => $measure));
			}
			$this->view->close_tag();

			$this->view->open_tag("labels");
			foreach (array_reverse($this->model->risk_matrix_labels) as $label) {
				$this->view->add_tag("label", ucfirst($label));
			}
			$this->view->close_tag();

			$this->view->open_tag("systems");
			foreach ($systems as $system) {
				$this->view->record($system, "system");
			}
			$this->view->close_tag();

			$this->view->add_tag("done", $progress["done"]);
			$this->view->add_tag("overdue", $progress["overdue"]);
			$this->view->add_tag("pending", $progress["pending"]);
			$this->view->add_tag("idle", $progress["idle"]);

			$this->view->close_tag();
		}
	}
?>
