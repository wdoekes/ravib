<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/splitform.xslt" />


<!--
//
//  Layout templates
//
//-->
<xsl:template name="splitform_header">
<h1>Aanmelden</h1>
</xsl:template>

<xsl:template name="splitform_footer">
</xsl:template>

<!--
//
//  E-mail form template
//
//-->
<xsl:template match="splitform/form_email">
<p>Het aanmeldproces bestaat uit een aantal stappen. In de eerste stap geef je je e-mailadres op. Daar wordt een verificatiecode heen gestuurd, die je in stap twee ter controlle weer invoert. In de derde en laatste stap voer je de overige benodigde gegevens in.</p>
<label for="email">E-mailadres:</label>
<input type="input" id="email" name="email" value="{email}" class="form-control" />
</xsl:template>

<!--
//
//  Code form template
//
//-->
<xsl:template match="splitform/form_code">
<p>Een e-mail met daarin een verificatiecode is naar het opgegeven e-mailadres opgestuurd.</p>
<label for="code">Verificatiecode:</label>
<input type="text" name="code" value="{code}" class="form-control" />
</xsl:template>

<!--
//
//  Account form template
//
//-->
<xsl:template match="splitform/form_account">
<label for="fullname">Volledige naam:</label>
<input type="input" id="fullname" name="fullname" value="{fullname}" class="form-control" />
<label for="username">Gebruikersnaam:</label>
<input type="input" id="username" name="username" value="{username}" class="form-control" style="text-transform:lowercase" />
<label for="password">Wachtwoord:</label>
<input type="password" id="password" name="password" class="form-control" />
<xsl:if test="../../../ask_organisation='yes'">
<label for="organisation">Organisatie:</label>
<input type="text" id="organisation" name="organisation" value="{organisation}" class="form-control" />
</xsl:if>
</xsl:template>

<!--
//
//  Process template
//
//-->
<xsl:template match="submit">
<xsl:call-template name="splitform_header" />
<xsl:call-template name="progressbar" />
<p>Je account is aangemaakt. Je kan nu inloggen.</p>
<xsl:call-template name="redirect"><xsl:with-param name="url" /></xsl:call-template>
</xsl:template>

</xsl:stylesheet>
