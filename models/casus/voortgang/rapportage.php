<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_voortgang_rapportage_model extends ravib_model {
		private function get_mitigation($case_id) {
			$query = "select g.* from case_threat_measure g, case_threats t where g.case_threat_id=t.id and t.case_id=%d";
			if (($controls = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($controls as $control) {
				$id = $control["case_threat_id"];

				if (is_array($result[$id] ?? false) == false) {
					$result[$id] = array();
				}

				array_push($result[$id], $control["measure_id"]);
			}

			return $result;
		}

		private function get_progress($case_id) {
			$query = "select * from case_progress where case_id=%d";

			return $this->db->execute($query, $case_id);
		}

		public function generate_report($case) {
			if (($standard = $this->get_standard($case["standard_id"])) == false) {
				return false;
			}

			if (($threats = $this->borrow("casus/dreigingen")->get_case_threats($case["id"])) === false) {
				return false;
			}

			if (($measures = $this->borrow("casus/voortgang")->get_case_measures($case["id"])) === false) {
				return false;
			}

			if (($measure_categories = $this->borrow("casus/voortgang")->get_measure_categories($case["standard_id"])) === false) {
				return false;
			}

			if (($mitigation = $this->get_mitigation($case["id"])) === false) {
				return false;
			}

			if (($progress = $this->get_progress($case["id"])) === false) {
				return false;
			}

			foreach ($threats as $t => $threat) {
				$threats[$t]["risk_value"] = $this->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
				$threats[$t]["relevant"] = ($threat["handle"] != THREAT_ACCEPT) && ($threat["handle"] > 0);
			}

			/* Generate report
			 */
			$pdf = new RAVIB_report($case["title"]);
			$pdf->SetAuthor($this->get_organisation($this->user->organisation_id)." en RAVIB");
			$pdf->SetSubject("Voortgangsrapportage aanpak informatiebeveiliging");
			$pdf->SetKeywords("RAVIB, informatiebeveiliging, voortgangsrapportage");
			$pdf->AliasNbPages();

			/* Title
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Titelpagina");
			$pdf->SetFont("helvetica", "B", 16);
			$pdf->Ln(100);
			$pdf->Cell(0, 0, "Voortgangsrapportage aanpak informatiebeveiliging", 0, 1, "C");
			$pdf->SetFont("helvetica", "", 12);
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $case["organisation"]." :: ".$case["name"], 0, 1, "C");
			$pdf->Ln(10);
			$pdf->Cell(0, 0, date_string("j F Y"), 0, 1, "C");
			$pdf->Image("images/layout/ravib_logo.png", 140, 20, 50, 0);

			/* Progress
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Voortgang");
			$pdf->AddChapter("Voortgang");
			$pdf->Ln(3);

			$pdf->Write(5, "De voortgang van de implementatie van de ".count($measures)." maatregelen, met in het groen het aantal afgeronde implementaties, in het geel het aantal nog lopende implementaties, in het rood het aantal implementaties waarvan de deadline is verstreken en in het zwart het aantal implementaties dat nog niet is toegekend aan een persoon.");
			$pdf->Ln(12);

			$today = time();
			$done = $pending = $overdue = $idle = 0;
			foreach ($measures as $i => $measure) {
				if ($measure["done"]) {
					$done++;
				} else if ($measure["deadline"] == null) {
					$idle++;
				} else if ($measure["deadline"] < $today) {
					$overdue++;
					$measures[$i]["overdue"] = true;
				} else {
					$pending++;
				}
			}

			if (($total = count($measures)) > 0) {
				$done = round(100 * $done / $total, 1);
				$overdue = round(100 * $overdue / $total, 1);
				$pending = round(100 * $pending / $total, 1);
				$idle = round(100 * $idle / $total, 1);
			} else {
				$done = $overdue = $pending = $idle = 0;
			}

			if (($delta = ($done + $overdue + $pending + $idle - 100)) != 0) {
				$values = array(
					"done" => $done,
					"overdue" => $overdue,
					"pending" => $pending,
					"idle"	=> $idle);
				arsort($values);
				$key = key($values);
				$$key -= $delta;
			}

			if ($done > 0) {
				$pdf->SetFillColor(0, 192, 0);
				$pdf->Cell(1.75 * $done, 5, $done."%", 1, 0, "C", true);
			}
			if ($pending > 0) {
				$pdf->SetFillColor(255, 192, 0);
				$pdf->Cell(1.75 * $pending, 5, $pending."%", 1, 0, "C", true);
			}
			if ($overdue > 0) {
				$pdf->SetFillColor(255, 0, 0);
				$pdf->Cell(1.75 * $overdue, 5, $overdue."%", 1, 0, "C", true);
			}
			if ($idle > 0) {
				$pdf->SetTextColor(255, 255, 255);
				$pdf->SetFillColor(0, 0, 0);
				$pdf->Cell(1.75 * $idle, 5, $idle."%", 1, 0, "C", true);
				$pdf->SetTextColor(0, 0, 0);
			}

			$planned = $invested = 0;
			foreach ($progress as $task) {
				$planned += $task["hours_planned"];
				$invested += $task["hours_invested"];
			}

			$pdf->Ln(10);
			$pdf->Write(5, "Totaal aantal geplande uren: ".$planned.". Totaal aantal geïnvesteerde uren: ".$invested.".");

			$pdf->Ln(20);
			$pdf->Write(5, "Onderstaand overzicht geeft per geraakt hoofdstuk uit de ".$standard." standaard het aantal afgeronde taken aan.");
			$pdf->Ln(12);

			/* Progress per chapter
			 */
			$categories = array();
			foreach ($measures as $i => $measure) {
				list($section) = explode(".", $measure["number"]);
				if (isset($categories[$section]) == false) {
					$categories[$section] = array(0, 0);
				}
				if ($measure["done"]) {
					$categories[$section][0]++;
				}
				$categories[$section][1]++;
			}
			ksort($categories);

			foreach ($categories as $key => $category) {
				$percentage = round(100 * $category[0] / $category[1]);
				$pdf->Write(6, $key.". ".$measure_categories[$key].": ".$percentage."% (".$category[0]." / ".$category[1].")");
				$pdf->Ln(6);
				if ($percentage > 0) {
					$pdf->SetFillColor(92, 92, 255);
					$pdf->Cell(1.75 * $percentage, 3, "", 1, 0, "C", true);
				}
				if ($percentage < 100) {
					$pdf->SetFillColor(255, 255, 255);
					$pdf->Cell(175 - 1.75 * $percentage, 3, "", 1, 0, "C", true);
				}
				$pdf->Ln(6);
			}

			/* Risk matrix
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Risicomatrices");
			$pdf->AddChapter("Risicomatrices");
			$pdf->Write(5, "De oorspronkelijke risicomatrix.");
			$pdf->Ln(10);

			$this->borrow("casus/rapportage")->draw_risk_matrix($pdf, $threats, true);

			foreach ($mitigation as $threat_id => $measures) {
				foreach ($progress as $item) {
					if (in_array($item["measure_id"], $measures) == false) {
						continue;
					}
					if (is_true($item["done"])) {
						$measures = array_diff($measures, array($item["measure_id"]));
					}
				}

				$mitigation[$threat_id]["done"] = (count($measures) == 0);
			}

			foreach ($threats as $threat) {
				if (is_true($mitigation[$threat["id"]]["done"] ?? false)) {
					unset($threats[$t]);
				}
			}

			$pdf->Ln(20);
			$pdf->Write(5, "De risicomatrix waarin de voltooide actiepunten zijn verwerkt.");
			$pdf->Ln(10);

			$this->borrow("casus/rapportage")->draw_risk_matrix($pdf, $threats, true);

			return $pdf;
		}
	}
?>
