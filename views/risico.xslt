<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Access code template
//
//-->

<xsl:template match="access_code">
<xsl:call-template name="show_messages" />

<form action="/{/output/page}" method="post">
<label for="access_code">Toegangscode:</label>
<input type="text" id="access_code" name="access_code" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Inloggen" class="btn btn-default" />
</div>
</form>
</xsl:template>

<!--
//
//  Risk template
//
//-->
<xsl:template match="risk">
<xsl:call-template name="show_messages" />

<p>Geef uw inschatting van de kans en impact van de dreiging zoals beschreven door de sessiebegeleider.</p>
<form action="/{/output/page}" method="post">
<div class="input-group">
<h3>Kans:</h3>
<xsl:for-each select="chance/option"><label for="chance_{@value}" class="option"><input id="chance_{@value}" type="radio" name="chance" value="{@value}" /><xsl:value-of select="." /></label></xsl:for-each>
</div>
<div class="input-group">
<h3>Impact:</h3>
<xsl:for-each select="impact/option"><label for="impact_{@value}" class="option"><input id="impact_{@value}" type="radio" name="impact" value="{@value}" /><xsl:value-of select="." /></label></xsl:for-each>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="Opsturen" class="btn btn-default" />
</div>
<div class="btn-group right">
<input type="submit" name="submit_button" value="Nieuwe toegangscode" class="btn btn-default" onClick="javascript:return confirm('Weet u het zeker?')" />
</div>
</form>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="access_code" />
<xsl:apply-templates select="risk" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
