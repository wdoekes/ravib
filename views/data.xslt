<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<div class="row">
<div class="col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Data exporteren</div>
<div class="panel-body">
<form action="/{/output/page}" method="post">
<input type="submit" name="submit_button" value="Exporteren" class="btn btn-default" />
</form>
</div>
</div>

</div>
<div class="col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Data importeren</div>
<div class="panel-body">
<form action="/{/output/page}" method="post" enctype="multipart/form-data">
<div class="input-group">
<span class="input-group-btn"><label class="btn btn-default">
<input type="file" name="file" style="display:none" class="form-control" onChange="$('#upload-file-info').val(this.files[0].name)" />Browse</label></span>
<input type="text" id="upload-file-info" readonly="readonly" class="form-control" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Importeren" class="btn btn-default" onClick="javascript:return confirm('Deze actie vervangt de huidige data (BIA, actoren en casussen) door de import. Doorgaan?')" /></span>
</div>
</form>
</div>
</div>

</div>
</div>

<div class="btn-group">
<a href="/dashboard" class="btn btn-default">Terug</a>
</div>

<div id="help">
<p>U bent zelf verantwoordelijk voor het veiligstellen van uw risicoanalyses. Maak daarom na het uitvoeren van een risicoanalyse altijd een export van uw data. Een export bevat de BIA's, de actoren en de risicoanalyses. Gebruikersgegevens worden niet geëxporteerd.</p>
<p>Bij het importeren van een RAVIB-export worden de aanwezig BIA, actoren en risicoanalyses eerst verwijderd.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/data.png" class="title_icon" />
<h1>Databeheer</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
