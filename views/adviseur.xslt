<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />

<table class="table table-condensed">
<thead>
<tr><th>Organisatie</th><th>Status</th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="role">
<tr>
<td><xsl:value-of select="." /></td>
<td><xsl:if test="@ready='no'">In aanvraag</xsl:if><xsl:if test="@ready='yes'"><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="Activeer" class="btn btn-primary btn-xs" /></form></xsl:if><xsl:if test="@ready='active'"><form action="/{/output/page}" method="post"><input type="submit" name="submit_button" value="Deactiveer" class="btn btn-info btn-xs" /></form></xsl:if></td>
<td><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="Verwijder" class="btn btn-danger btn-xs" onClick="javascript:return confirm('VERWIJDER: Weet u het zeker?')" /></form></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2>Adviseursrol aanvragen</h2>
<p>Om de adviseursrol voor een organisatie aan te vragen, geeft u hieronder de RAVIB-gebruikersnaam of het e-mailadres op van uw contactpersoon bij de organisatie waar u als adviseur voor wilt optreden.</p>
<form action="/{/output/page}" method="post" class="request">
<div class="input-group">
<input type="text" id="user" name="user" placeholder="Gebruikersnaam of e-mailadres" class="form-control datepicker" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Aanvragen" class="btn btn-default" /></span>
</div>
</form>

<div class="btn-group">
<a href="/dashboard" class="btn btn-default">Terug</a>
</div>

<div id="help">
<p>In RAVIB is het mogelijk om voor een andere organisatie op te treden als adviseur voor informatiebeveiliging. Op deze pagina kunt u een verzoek indienen om toegang te krijgen tot de risicoanalyses van die andere organisatie.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/adviseur.png" class="title_icon" />
<h1>Adviseursrol</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
