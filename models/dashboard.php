<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class dashboard_model extends ravib_model {
		private function all_done($progress) {
			if (count($progress) == 0) {
				return false;
			}

			foreach ($progress as $action) {
				if (is_false($action["done"])) {
					return false;
				}
			}

			return true;
		}

		public function get_threats() {
			$query = "select t.id, t.chance, t.impact, c.id as case_id ".
			         "from case_threats t, cases c ".
			         "where t.case_id=c.id and c.organisation_id=%d and c.archived=%d ".
			         "and t.handle!=%d and t.handle!=%d";

			if (($threats = $this->db->execute($query, $this->organisation_id, NO, 0, THREAT_ACCEPT)) === false) {
				return false;
			}

			$query = "select p.done from case_threat_measure l ".
			         "left join case_progress p on p.case_id=%d and p.measure_id=l.measure_id ".
			         "where l.case_threat_id=%d";

			$result = array(0, 0, 0, 0);
			foreach ($threats as $threat) {
				if (($progress = $this->db->execute($query, $threat["case_id"], $threat["id"])) === false) {
					return false;
				}

				if ($this->all_done($progress)) {
					continue;
				}

				$risk_value = $this->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
				$result[$risk_value]++;
			}

			return array_reverse($result, true);
		}

		public function get_measures() {
			$query = "select m.id, t.chance, t.impact, c.id as case_id ".
			         "from case_threat_measure l, case_threats t, cases c, measures m ".
			         "where m.id=l.measure_id and l.case_threat_id=t.id and t.case_id=c.id and c.standard_id=m.standard_id ".
			         "and c.organisation_id=%d and c.archived=%d and t.handle!=%d and t.handle!=%d";

			if (($measures = $this->db->execute($query, $this->organisation_id, NO, 0, THREAT_ACCEPT)) === false) {
				return false;
			}

			$query = "select done from case_progress c where case_id=%d and measure_id=%d";

			$urgency = array();
			foreach ($measures as $measure) {
				if (($progress = $this->db->execute($query, $measure["case_id"], $measure["id"])) === false) {
					return false;
				}

				if ($this->all_done($progress)) {
					continue;
				}

				$urgency_value = $this->risk_matrix[$measure["chance"] - 1][$measure["impact"] - 1];
				if ($urgency[$measure["id"]] ?? null == null) {
					$urgency[$measure["id"]] = $urgency_value;
				} else if ($urgency_value > $urgency[$measure["id"]]) {
					$urgency[$measure["id"]] = $urgency_value;
				}
			}

			$urgency = array_count_values($urgency);

			$result = array(0, 0, 0, 0);
			for ($i = 0; $i < 4; $i++) {
				$result[$i] += $urgency[$i] ?? 0;
			}

			return array_reverse($result, true);
		}

		private function sort_bia($bia1, $bia2) {
			return strcmp($bia1["item"], $bia2["item"]);
		}

		public function get_bia() {
			$query = "select * from bia where organisation_id=%d";

			if (($items = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			foreach (array_keys($items) as $key) {
				$this->decrypt($items[$key], "item");
			}

			uasort($items, array($this, "sort_bia"));

			$risk_matrix = config_array(RISK_MATRIX);

			$query_r = "select t.chance, t.impact, c.id as case_id, t.id ".
			           "from case_threats t, case_threat_bia b, cases c ".
			           "where t.id=b.case_threat_id and t.case_id=c.id and b.bia_id=%d and c.archived=%d";

			$query_p = "select p.done from case_threat_measure l ".
			           "left join case_progress p on p.case_id=%d and p.measure_id=l.measure_id ".
			           "where l.case_threat_id=%d";

			foreach ($items as $i => $item) {
				$items[$i]["risk0"] = 0;
				$items[$i]["risk1"] = 0;
				$items[$i]["risk2"] = 0;
				$items[$i]["risk3"] = 0;

				if (($risks = $this->db->execute($query_r, $item["id"], NO)) === false) {
					return false;
				}

				foreach ($risks as $risk) {
					if (($progress = $this->db->execute($query_p, $risk["case_id"], $risk["id"])) === false) {
						return false;
					}

					if ($this->all_done($progress)) {
						continue;
					}

					$risk_value = $this->risk_matrix[$risk["chance"] - 1][$risk["impact"] - 1];
					$items[$i]["risk".$risk_value]++;
				}
			}

			return $items;
		}

		public function get_progress() {
			$query = "select distinct s.measure_id from case_threat_measure s, case_threats t, measures m, cases c ".
			         "where s.case_threat_id=t.id and t.case_id=c.id and c.organisation_id=%d and c.archived=%d ".
			         "and s.measure_id=m.id and m.standard_id=c.standard_id and t.handle!=%d and t.handle!=%d";
			if (($result = $this->db->execute($query, $this->organisation_id, NO, 0, THREAT_ACCEPT)) === false) {
				return false;
			}
			$total = count($result);

			$measures = array();
			foreach ($result as $item) {
				array_push($measures, $item["measure_id"]);
			}

			$result = array(
				"done"    => 0,
				"overdue" => 0,
				"pending" => 0,
				"idle"    => 0);

			if ($total == 0) {
				return $result;
			}

			$query = "select p.*, UNIX_TIMESTAMP(p.deadline) as deadline from case_progress p, cases c ".
					 "where p.case_id=c.id and c.organisation_id=%d";
			if (($progress = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			$today = strtotime("today");

			foreach ($progress as $item) {
				if (in_array($item["measure_id"], $measures) == false) {
					continue;
				}
				if (is_true($item["done"])) {
					$result["done"]++;
				} else if ($item["deadline"] == null) {
					$result["idle"]++;
				} else if ($item["deadline"] < $today) {
					$result["overdue"]++;
				} else {
					$result["pending"]++;
				}
			}

			$result["idle"] = $total - $result["done"] - $result["overdue"] - $result["pending"];

			foreach (array_keys($result) as $key) {
				$result[$key] = round(100 * $result[$key] / $total, 1);
			}

			return $result;
		}
	}
?>
