<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs threats">
<thead class="table-xs">
<tr>
<th>Dreiging</th>
<th>Aanpak</th>
<th>Urgentie</th>
<th>Maatregelen</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="threats/threat">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{/output/content/case/@id}/{@id}'">
<td><span class="table-xs">Dreiging</span><xsl:value-of select="threat" /></td>
<td><span class="table-xs">Aanpak</span><xsl:value-of select="handle" /></td>
<td><span class="table-xs">Urgentie</span><span class="urgency{risk_value}"><xsl:value-of select="risk_label" /></span></td>
<td><span class="table-xs">Maatregelen</span><xsl:value-of select="measures" /><xsl:if test="(measures=0 and handle!='accepteren') or (measures>0 and handle='accepteren')"><img src="/images/warning.png" class="warning" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<xsl:if test="count(threats/threat)>0">
<div class="btn-group right">
<a href="/casus/scenarios/{../case/@id}" class="btn btn-default">Naar scenario's</a>
</div>
</xsl:if>

<div id="help">
<p>In de kolom Maatregelen kan een uitroepteken-icoontje verschijnen. Dat gebeurt als voor een niet-geaccepteerde dreiging nog geen maatregelen zijn geselecteerd. Het gebeurt ook als u een dreiging accepteert, maar daar toch maatregelen voor selecteert.</p>
<p>Maatregelen voor geaccepteerde dreigingen worden niet meegenomen in de rapportage en in de voortgang-module!</p>
<p>Voor dreigingen die zijn aangemaakt op basis van een template, zijn vanuit RAVIB maatregelen geselecteerd. Zie deze maatregelen als een suggestie vanuit RAVIB. Het dringende advies aan u is om door deze selectie heen te lopen om te zien of deze inderdaad voor uw situatie van toepassing zijn.</p>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<h2><xsl:value-of select="threat/threat" /></h2>
<div class="row">
<div class="col-sm-4"><label>Kans:</label> <xsl:value-of select="threat/chance" /></div>
<div class="col-sm-4"><label>Impact:</label> <xsl:value-of select="threat/impact" /></div>
<div class="col-sm-4"><label>Gekozen aanpak:</label> <xsl:value-of select="threat/handle" /></div>
</div>
<label>Huidige situatie / maatregelen:</label>
<p class="measure"><xsl:value-of select="threat/current" /></p>
<label>Gewenste situatie / te nemen acties:</label>
<p class="measure"><xsl:value-of select="threat/action" /></p>

<form action="/{/output/page}/{../case/@id}" method="post">
<input type="hidden" name="case_threat_id" value="{threat/@id}" />
<h2><xsl:value-of select="measures/@standard" /><span class="show_highlighter" onClick="javascript:$('div.highlighter').toggle()">+</span></h2>
<div class="row highlighter">
<div class="col-md-3 col-sm-4 col-xs-12">Accentueer maatregelen voor:</div>
<div class="col-md-9 col-sm-8 col-xs-12"><select class="form-control threats" onChange="javascript:highlight(this)">
<xsl:for-each select="threats/threat">
<option value="{@id}"><xsl:value-of select="." /></option>
</xsl:for-each>
</select></div>
</div>
<table class="table table-condensed table-striped measures">
<thead>
<tr>
<th>#</th>
<th>Maatregel</th>
<th>Vermindert</th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="measures/measure">
<tr class="measure_{@id}"><xsl:if test="effective='no'"><xsl:attribute name="class">ineffective</xsl:attribute></xsl:if>
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="reduce" /></td>
<td><input type="checkbox" name="selected[]" value="{@id}"><xsl:if test="selected='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<input type="submit" name="submit_button" value="Opslaan" class="btn btn-default" />
<a href="/{/output/page}/{../case/@id}" class="btn btn-default">Afbreken</a>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Maatregelen</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
