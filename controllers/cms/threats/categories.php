<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_threats_categories_controller extends Banshee\tablemanager_controller {
		protected $name = "Threat category";
		protected $back = "cms/threats";
		protected $icon = "threat_categories.png";
		protected $browsing = null;
	}
?>
